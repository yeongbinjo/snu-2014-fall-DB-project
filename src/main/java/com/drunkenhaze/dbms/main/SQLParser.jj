options {
  STATIC = false;
  UNICODE_INPUT = true;
}

PARSER_BEGIN(SQLParser)
package com.drunkenhaze.dbms.main;

import java.util.List;
import java.util.LinkedList;
import java.util.Date;
import java.lang.String;
import java.lang.Integer;
import java.io.FileInputStream;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import org.apache.commons.lang3.tuple.Pair;
import com.drunkenhaze.dbms.components.*;
import com.drunkenhaze.dbms.exceptions.*;
import com.drunkenhaze.dbms.managers.*;
import com.drunkenhaze.dbms.operators.*;

class SQLParser {
	private final List<String> prohibitKeywords = new LinkedList<String>();
	private FileManager fileMgr = null;

	public void setFileManager(FileManager _fileMgr) {
		fileMgr = _fileMgr;
	}

	public void init() {
		initProhibitKeywords();
	}

	private void initProhibitKeywords() {
		// LEGAL IDENTIFIER가 될 수 없는 키워드들을 갖고 있는 리스트를 초기화
		// 아마 이런 체크 자체가 필요 없을 수도...
		prohibitKeywords.add("select");
		prohibitKeywords.add("from");
		prohibitKeywords.add("where");
		prohibitKeywords.add("create");
		prohibitKeywords.add("table");
		prohibitKeywords.add("is");
		prohibitKeywords.add("not");
		prohibitKeywords.add("and");
		prohibitKeywords.add("or");
		prohibitKeywords.add("insert");
		prohibitKeywords.add("into");
		prohibitKeywords.add("delete");
		prohibitKeywords.add("null");
		prohibitKeywords.add("desc");
		prohibitKeywords.add("drop");
		prohibitKeywords.add("show");
		prohibitKeywords.add("tables");
		prohibitKeywords.add("values");
		prohibitKeywords.add("as");
		prohibitKeywords.add("primary");
		prohibitKeywords.add("foreign");
		prohibitKeywords.add("key");
		prohibitKeywords.add("into");
		prohibitKeywords.add("int");
		prohibitKeywords.add("char");
		prohibitKeywords.add("date");
		prohibitKeywords.add("references");
		prohibitKeywords.add("exit");
		prohibitKeywords.add("drunkenhazeDatabase");
	}

	private void printTableNames(List<String> tableNames) {
		// SHOW TABLES 구문을 위한 출력 함수
		for (int i = 0; i < 30; i++) System.out.print("-");
		System.out.println();
		for (String tableName : tableNames) {			System.out.println(tableName);		}
		for (int i = 0; i < 30; i++) System.out.print("-");
		System.out.println();
	}

	public boolean isStreamEmpty() {
		int i = jj_input_stream.bufpos + 1;
		//System.out.println("<pos: " + i + ", maxNextCharInd: " + jj_input_stream.maxNextCharInd);
		for (i = jj_input_stream.bufpos + 1; i < jj_input_stream.maxNextCharInd; i++) {
			switch(jj_input_stream.buffer[i]) {
			case ' ':
			case '\t':
			case '\r':
			case '\n':
				break;
			default:
				return false;			}			
		}
		//System.out.println(">pos: " + i + ", maxNextCharInd: " + jj_input_stream.maxNextCharInd);
		return true;	}

	public void printShell(String msg) {
		// 쉘 출력
		if ( msg != null ) {			System.out.println("SQL_2008-11725> " + msg);
		} else {			printShell();
		}	}

	public void printShell() {
		// 빈 쉘 출력
		System.out.println("SQL_2008-11725> ");
	}

	public void errorSkipTo(int kind) {
		// 만약 symantic error가 발생할 경우 다음 statement까지 (보통 다음 SEMICOLON까지) 스킵한다.
		// 아마 PRJ 1-1 에서는 필요없을 기능 
		Token t;
		do {
			t = getNextToken();
		} while (t.kind != kind);
	}

	private boolean checkLegal(Token t) {
		// 구문 해석기로 가져온 토큰이 Legal Identifier인지 판단한다.
		String legalId = t.image.toLowerCase();
		for (String keyword : prohibitKeywords) {
			if (legalId.equals(keyword)) {
				return false;
			}		}
		return true;
	}
}
PARSER_END(SQLParser)

////////////////////////////////////////////////////////////////
// LEXICAL ANALYZER - TOKEN DEFINITION
////////////////////////////////////////////////////////////////
SPECIAL_TOKEN: {"\t" | " " | "\n" | "\r" | "\r\n"}
TOKEN: {
	< SEMICOLON				: ";" >
|	< PERIOD				: "." >
|	< ASTERISK				: "*" >
|	< EXIT					: "exit" >
|	< CREATE				: "create" >
|	< DROP					: "drop" >
|	< TABLE 				: "table" >
|	< TABLES				: "tables" >
|	< SHOW					: "show" >
|	< DESC					: "desc" >
|	< SELECT				: "select" >
|	< INSERT				: "insert" >
|	< INTO					: "into" >
|	< FROM					: "from" >
|	< DELETE				: "delete" >
|	< PRIMARY				: "primary" >
|	< FOREIGN				: "foreign" >
|	< KEY					: "key" >
|	< REFERENCES			: "references" >
|	< IS					: "is" >
|	< NOT					: "not" >
|	< NULL					: "null" >
|	< VALUES				: "values" >
|	< AS					: "as" >
|	< OR					: "or" >
|	< AND					: "and" >
|	< WHERE					: "where" >
|	< INT					: "int" >
|	< CHAR					: "char" >
|	< DATE					: "date" >
|	< BLOWUP				: "blowup" >
}
TOKEN: {
	< #_ALPHABET			: ["a"-"z","A"-"Z"] >
|	< #_QUOTE				: "'" >
|	< #_NONQUOTECHAR		: ~["'","\n","\r","\t","\""] >
|	< #_SIGN				: ("+"|"-") >
|	< #_DIGIT				: ["0"-"9"] >
|	< #_INTVALUE			: (< _SIGN >)?(< _DIGIT >)+ >
|	< #_CHARSTRING			: < _QUOTE >(< _NONQUOTECHAR >)*< _QUOTE > >
|	< #_DATEVALUE			: (< _DIGIT >){4}"-"(< _DIGIT >){2}"-"(< _DIGIT >){2} >
|	< #_UNDERSCORE			: "_" >
|	< #_LEFTPAREN			: "(" >
|	< #_RIGHTPAREN			: ")" >
}
TOKEN: {		
	< INTVALUE				: < _INTVALUE > >
|	< CHARSTRING			: < _CHARSTRING > >
|	< DATEVALUE				: < _DATEVALUE > > 
|	< LEFTPAREN 			: < _LEFTPAREN > >
|	< RIGHTPAREN			: < _RIGHTPAREN > >
|	< COMMA					: "," >
|	< COMPOP				: ("<"|">"|"="|">="|"<="|"!=") >
|	< LEGALID				: < _ALPHABET >(< _ALPHABET >|< _UNDERSCORE >)* >	// same as LEGAL IDENTIFIER
}

////////////////////////////////////////////////////////////////
// CREATE TABLE
////////////////////////////////////////////////////////////////
Token getLegalIdentifier() throws SemanticError:
{	Token legalIdentifier;
}
{
	legalIdentifier = < LEGALID >
	{
		// 받아온 토큰이 Legal Identifier인지 blacklist sanitization 통해 확인하고,
		// 만약 매칭되는 keyword가 있다면 Legal Identifier가 아니므로 syntax error 처리를 위해 ParseException을 던진다. 
		if (!checkLegal(legalIdentifier)) {
			throw new ParseException("\"" + legalIdentifier.image + "\" is not legal identifier.");
		}
	}
	{		return legalIdentifier;
	}}

List<String> getColumnNameList() throws SemanticError:
{
	Token tColumnName;
}{
	{
		List<String> columnNames = new LinkedList<String>();
	}	< LEFTPAREN >
	tColumnName = getLegalIdentifier()
	{ columnNames.add(tColumnName.image); }
	(		< COMMA >
		tColumnName = getLegalIdentifier()
		{ columnNames.add(tColumnName.image); }
	)*
	< RIGHTPAREN >
	{ return columnNames; }
}

PrimaryKey getPrimaryKeyConstraint(TableSchema tableSchema) throws SemanticError:
{
	List<String> columnNames;
}
{	< PRIMARY >
	< KEY >
	columnNames = getColumnNameList()
	{ return new PrimaryKey(tableSchema, columnNames); }
}

ForeignKey getReferentialConstraint(TableSchema tableSchema) throws SemanticError:
{
	List<String> columnNames;
	List<String> refColumnNames;
	Token tTableName;
	Token tReferences;}
{	< FOREIGN >
	< KEY >
	columnNames = getColumnNameList()
	tReferences = < REFERENCES >
	{
		if (tReferences.specialToken == null) {
			throw new ParseException(Messages.SpaceBetweenReferences);
		}
	}
	tTableName = getLegalIdentifier()
	refColumnNames = getColumnNameList()
	{ return new ForeignKey(columnNames, tTableName.image, refColumnNames); }
}

void getTableConstraintDefinition(TableSchema tableSchema) throws SemanticError:
{
	PrimaryKey pk;
	ForeignKey fk;}
{	pk = getPrimaryKeyConstraint(tableSchema)
	{ tableSchema.setPrimaryKey(pk); }
|
	fk = getReferentialConstraint(tableSchema)
	{ tableSchema.addForeignKey(fk); }
}

void getColumnDefinition(TableSchema tableSchema) throws SemanticError:
{
	Token tColumnName;
	Token tInt;
	Token tNot;
	ColumnDefinition cd;
	ColumnDataType dt;}
{
	tColumnName = getLegalIdentifier()
	(
		< INT >
		{ dt = new IntDataType(); }
	|
		< CHAR >
		< LEFTPAREN >
		tInt = < INTVALUE >
		< RIGHTPAREN >
		{ dt = new CharDataType(Integer.parseInt(tInt.image)); }
	|
		< DATE >
		{ dt = new DateDataType(); }
	)
	{ cd = new ColumnDefinition(tColumnName.image, dt); }
	(
		tNot = < NOT >
		{			if (tNot.specialToken == null) {
				throw new ParseException(Messages.SpaceBetweenNotNull);			}
		}
		< NULL >
		{ cd.setNotNull(); }
	)?
	{ tableSchema.addColumnDefinition(cd); }
}

void getTableElement(TableSchema tableSchema) throws SemanticError:
{}
{
	(		getColumnDefinition(tableSchema)
	|
		getTableConstraintDefinition(tableSchema)
	)
}

void getTableElementList(TableSchema tableSchema) throws SemanticError:
{}
{	< LEFTPAREN >
	getTableElement(tableSchema)
	(		< COMMA >
		getTableElement(tableSchema)
	)*
	< RIGHTPAREN >
}

void createTable() throws SemanticError:
{
	Token tTableName;
	TableSchema tableSchema;}
{
	< CREATE >
	< TABLE >
	tTableName = getLegalIdentifier()
	{		tableSchema = new TableSchema(tTableName.image);
	}
	getTableElementList(tableSchema)
	< SEMICOLON >
	{
		// 테이블이 성공적으로 생성되었으면 FileManager 통해 파일에 테이블 스키마 정보를 쓴다.
		if (fileMgr != null) {
			tableSchema.verify();			tableSchema.save();
		}
		printShell(String.format(Messages.CreateTableSuccess, tTableName.image));
	}}

////////////////////////////////////////////////////////////////
// DROP TABLE
////////////////////////////////////////////////////////////////
void dropTable() throws SemanticError:{
	Token tTableName;
}
{
	< DROP >
	< TABLE >
	tTableName = getLegalIdentifier()
	< SEMICOLON >
	{
		if (fileMgr != null) {
			TableSchema table = TableSchema.load(tTableName.image);
			if (table == null) throw new SemanticError(Messages.NoSuchTable);	// 삭제하려는 테이블이 없는 경우
			table.drop();
			printShell(String.format(Messages.DropSuccess, table.getName()));
		} else {
			printShell(String.format(Messages.DropSuccess, tTableName.image));	// just for test
		}
	}
}

////////////////////////////////////////////////////////////////
// SHOW TABLES
////////////////////////////////////////////////////////////////
void showTables() throws SemanticError:
{
}
{
	< SHOW >
	< TABLES >
	< SEMICOLON >
	{
		if (fileMgr != null) {
			List<String> tableNames = fileMgr.getTableNames();
			if (tableNames.size() == 0) throw new SemanticError(Messages.ShowTablesNoTable);
			printTableNames(tableNames);
		} else printShell(String.format(Messages.Pr1_1TemporarySuccess, "show tables"));	// just for test
	}
}

////////////////////////////////////////////////////////////////
// DESC
////////////////////////////////////////////////////////////////
void desc() throws SemanticError:
{
	Token tTableName;
}
{
	< DESC >
	tTableName = getLegalIdentifier()
	< SEMICOLON >
	{
		if (fileMgr != null) {
			TableSchema tableSchema = TableSchema.load(tTableName.image);
			if (tableSchema == null) throw new SemanticError(Messages.NoSuchTable);
			System.out.println(tableSchema);
		}
		else printShell(String.format(Messages.Pr1_1TemporarySuccess, "desc"));	// just for test
	}
}

////////////////////////////////////////////////////////////////
// SELECT TABLE
////////////////////////////////////////////////////////////////
SelectedColumn getSelectedColumn() throws SemanticError:
{
	Token tPeriod = null;
	Token tColumnName = null;
	Token tTableName = null;
	Token tAlias = null;
	SelectedColumn curColumn = new SelectedColumn();}
{	(
		LOOKAHEAD(2)		tTableName = getLegalIdentifier()
		tPeriod = < PERIOD >
		{
			if (tPeriod.specialToken != null) {
				throw new ParseException(Messages.SpaceBetweenTableNameColumnName);
			}
			curColumn.setTableName(tTableName.image);
		}
	)?
	tColumnName = getLegalIdentifier()
	{
		if (tPeriod != null && tColumnName.specialToken != null) {
			throw new ParseException(Messages.SpaceBetweenTableNameColumnName);
		}
		curColumn.setColumnName(tColumnName.image);
	}
	(		< AS >
		tAlias = getLegalIdentifier()
		{ curColumn.setAlias(tAlias.image); }
	)?
	{ return curColumn; }
}

List<SelectedColumn> getSelectList() throws SemanticError:
{
	List<SelectedColumn> selectList = new LinkedList<SelectedColumn>();
	SelectedColumn curColumn;}
{
	(		< ASTERISK >
		{ selectList.add(new SelectedColumn("*", null, null)); }
	|
		curColumn = getSelectedColumn()
		{ selectList.add(curColumn); }
		(			< COMMA >
			curColumn = getSelectedColumn()
			{ selectList.add(curColumn); }
		)*
	)
	{ return selectList; }
}

Object getComparableValue() throws SemanticError:{
	Token tValue;
	Object value;
}
{
	(		tValue = < INTVALUE >
		{ value = Integer.parseInt(tValue.image); }
	|
		tValue = < CHARSTRING >
		{ value = tValue.image.substring(1, tValue.image.length() - 1); }
	|
		tValue = < DATEVALUE >
		{
			try {				value = Constants.dateFormat.parse(tValue.image);
			} catch (java.text.ParseException e) {
				throw new SemanticError(Messages.InvalidDateRangeError);			} 
			if (value == null) throw new SemanticError(Messages.InvalidDateRangeError);
		}
	)
	{ return value; }}

boolean getNullOperation() throws SemanticError:
{
	boolean result = true;}
{
	< IS >
	(
		< NOT >
		{ result = false; }	)?
	< NULL >
	{ return result; }
}

IsNullOperator getNullPredicate() throws SemanticError:
{
	Token tTableName = null;
	Token tPeriod = null;
	Token tColumnName = null;
	boolean checkIsNull;}
{
	(
		LOOKAHEAD(2)
		tTableName = getLegalIdentifier()
		tPeriod = < PERIOD >
		{
			if ( tPeriod.specialToken != null ) {
				throw new ParseException(Messages.SpaceBetweenTableNameColumnName);
			}
		}
	)?
	tColumnName = getLegalIdentifier()
	{
		if ( tPeriod != null && tColumnName.specialToken != null ) {
			throw new ParseException(Messages.SpaceBetweenTableNameColumnName);
		}
	}
	checkIsNull = getNullOperation()
	{
		return new IsNullOperator(
			new SelectedColumn(tColumnName.image,
				tTableName != null ? tTableName.image : null,
				null), checkIsNull);	}
}

Object getCompOperand() throws SemanticError:
{
	Token tTableName = null;
	Token tPeriod = null;
	Token tColumnName = null;
	Object obj;}
{
	(		obj = getComparableValue()
		{ return obj; }
	|
		(
			LOOKAHEAD(2)			tTableName = getLegalIdentifier()
			tPeriod = < PERIOD >
			{
				if ( tPeriod.specialToken != null ) {
					throw new ParseException(Messages.SpaceBetweenTableNameColumnName);
				}
			}
		)?
		tColumnName = getLegalIdentifier()
		{
			if ( tPeriod != null && tColumnName.specialToken != null ) {
				throw new ParseException(Messages.SpaceBetweenTableNameColumnName);
			}
		}
		{
			return new SelectedColumn(tColumnName.image,
				tTableName != null ? tTableName.image : null,
				null);
		}
	)
}

CompareOperator getComparisonPredicate() throws SemanticError:
{
	Object operand1;
	Object operand2;
	Token tCompOp;}
{	operand1 = getCompOperand()
	tCompOp = < COMPOP >
	operand2 = getCompOperand()
	{ return new CompareOperator(operand1, operand2, tCompOp.image); }
}

Operator getPredicate() throws SemanticError:
{
	Operator operator;}
{
	(
		LOOKAHEAD(4)
		operator = getComparisonPredicate()
	|
		operator = getNullPredicate()
	)
	{ return operator; }
}

SelectedTable getReferedTable() throws SemanticError:
{
	SelectedTable selectedTable = new SelectedTable();
	Token tTableName;
	Token tAlias;}
{	tTableName = getLegalIdentifier()
	{ selectedTable.setTableName(tTableName.image); }
	(		< AS >
		tAlias = getLegalIdentifier()
		{ selectedTable.setAlias(tAlias.image); }
	)?
	{ return selectedTable; }
}

List<SelectedTable> getTableReferenceList() throws SemanticError:
{
	List<SelectedTable> selectedTableList = new LinkedList<SelectedTable>();
	SelectedTable curTable;}
{	curTable = getReferedTable()
	{ selectedTableList.add(curTable); }
	(		< COMMA >
		curTable = getReferedTable()
		{ selectedTableList.add(curTable); }
	)*
	{ return selectedTableList; }
}

List<SelectedTable> getFromClause() throws SemanticError:
{
	List<SelectedTable> selectedTableList;}
{	< FROM >
	selectedTableList = getTableReferenceList()
	{ return selectedTableList; }
}

Operator getParenthesizedBooleanExpression() throws SemanticError:
{
	Operator operator;}
{
	< LEFTPAREN >
	operator = getBooleanValueExpression()
	< RIGHTPAREN >
	{ return operator; }}

Operator getBooleanTest() throws SemanticError:
{
	Operator operator;
}
{
	(
		operator = getPredicate()
	|
		operator = getParenthesizedBooleanExpression()
	)
	{ return operator; }
}

Operator getBooleanFactor() throws SemanticError:
{
	Operator operator;
	boolean checkNot = false;
}
{
	(
		< NOT >
		{ checkNot = true; }
	)?
	operator = getBooleanTest()
	{
		if (checkNot) return new NotOperator(operator);
		else return operator;
	}
}

Operator getBooleanTerm() throws SemanticError:
{
	Operator operator1;
	Operator operator2;
}
{
	operator1 = getBooleanFactor()
	(
		< AND >
		operator2 = getBooleanFactor()
		{ operator1 = new AndOperator(operator1, operator2); }
	)*	// change left recursion to loop
	{ return operator1; }
}

Operator getBooleanValueExpression() throws SemanticError:
{
	Operator operator1;
	Operator operator2;
}
{
	operator1 = getBooleanTerm()
	(	
		< OR >
		operator2 = getBooleanTerm()
		{ operator1 = new OrOperator(operator1, operator2); }
	)*	// change left recursion to loop
	{ return operator1; }
}

Operator getWhereClause() throws SemanticError:
{
	Operator operator;}
{	< WHERE >
	operator = getBooleanValueExpression()
	{ return operator; }
}

void select() throws SemanticError:
{
	List<SelectedColumn> selectedColumnList;
	List<SelectedTable> selectedTableList;
	Operator operator = null;}
{
	< SELECT >
	selectedColumnList = getSelectList()
	selectedTableList = getFromClause()
	(operator = getWhereClause())?
	< SEMICOLON >
	{
		if (fileMgr != null) {
			DMLManager.select(selectedColumnList, selectedTableList, operator);
		}
		else printShell(String.format(Messages.Pr1_1TemporarySuccess, "select"));
	}}


////////////////////////////////////////////////////////////////
// INSERT
////////////////////////////////////////////////////////////////
Object getValue() throws SemanticError:
{
	Object value;}
{
	(			< NULL >
		{ value = null; }
	|
		value = getComparableValue()
	)
	{ return value; }}

List<Object> getValueList() throws SemanticError:
{
	Object value;
	List<Object> valueList = new LinkedList<Object>();}
{	< VALUES >
	< LEFTPAREN >
	value = getValue()
	{ valueList.add(value); }
	(		< COMMA >
		value = getValue()
		{ valueList.add(value); }
	)*
	< RIGHTPAREN >
	{ return valueList; }
}

Pair<List<String>, List<Object>> getInsertColumnsAndSource() throws SemanticError:
{
	List<String> columnNameList = null;
	List<Object> valueList;}
{	(
		columnNameList = getColumnNameList()
	)?
	valueList = getValueList()
	{
		return Pair.of(columnNameList, valueList);	}
}

void insert() throws SemanticError:
{
	Token tTableName;
	Pair<List<String>, List<Object>> columnsAndSource;
}
{
	< INSERT >
	< INTO >
	tTableName = getLegalIdentifier()
	columnsAndSource = getInsertColumnsAndSource()
	< SEMICOLON >
	{
		if (fileMgr != null) {			
			DMLManager.insert(tTableName.image, columnsAndSource.getLeft(), columnsAndSource.getRight());
		}
		printShell(Messages.InsertResult);
	}}

////////////////////////////////////////////////////////////////
// DELETE
////////////////////////////////////////////////////////////////
void delete() throws SemanticError:
{
	Token tTableName;
	Operator operator = null;
}
{
	< DELETE >
	< FROM >
	tTableName = getLegalIdentifier()
	(operator = getWhereClause())?
	< SEMICOLON >
	{
		if (fileMgr != null) {
			Pair<Integer, Integer> deleteResult = DMLManager.delete(tTableName.image, operator);
			printShell(String.format(Messages.DeleteResult, deleteResult.getLeft()));
			if (!deleteResult.getRight().equals(0))
				printShell(String.format(Messages.DeleteReferentialIntegrityPassed, deleteResult.getRight()));
		} else printShell(String.format(Messages.Pr1_1TemporarySuccess, "delete"));
	}
}



////////////////////////////////////////////////////////////////
// QUERY
////////////////////////////////////////////////////////////////
void query() throws SemanticError:
{}{
	createTable()
|		dropTable()
|
	showTables()
|
	desc()
|
	select()
|
	insert()
|
	delete()
}

////////////////////////////////////////////////////////////////
// ENTRY
////////////////////////////////////////////////////////////////
boolean parse() throws SemanticError:
{}
{
	(
		query()
	|	
		< EXIT >
		< SEMICOLON >
		{ return false; }
	|
		< BLOWUP >		// custom command; delete database
		< SEMICOLON >
		{ if (fileMgr != null) fileMgr.blowup(); }
	)
	{ return true; }
}