package com.drunkenhaze.dbms.managers;

import java.util.Date;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.TreeMap;

import org.apache.commons.lang3.tuple.Pair;

import com.drunkenhaze.dbms.components.CharDataType;
import com.drunkenhaze.dbms.components.ColumnDefinition;
import com.drunkenhaze.dbms.components.DateDataType;
import com.drunkenhaze.dbms.components.IntDataType;
import com.drunkenhaze.dbms.components.Row;
import com.drunkenhaze.dbms.components.SelectedColumn;
import com.drunkenhaze.dbms.components.SelectedTable;
import com.drunkenhaze.dbms.components.TableSchema;
import com.drunkenhaze.dbms.exceptions.SemanticError;
import com.drunkenhaze.dbms.main.Constants;
import com.drunkenhaze.dbms.main.Messages;
import com.drunkenhaze.dbms.operators.CompareOperator;
import com.drunkenhaze.dbms.operators.Operator;
import com.google.common.collect.Sets;

public class DMLManager {
	/*
	 * INSERT, DELETE, SELECT 구문의 기능을 수행하는 클래스
	 */

	public static void select(List<SelectedColumn> selectedColumns,
			List<SelectedTable> selectedTables,
			Operator operator) throws SemanticError {
		// 확인과 동시에 select 대상 테이블의 스키마 리스트와 인덱스 맵을 구성한다
		// 인덱스 맵을 구성하는 이유는 테이블 이름 따른 테이블 순서를 알기 위함이다.
		// 테이블 순서는 cartesian product의 결과에서 몇 번째 element를 참조해야하는 지 알기 위해 필요한 정보이다.
		List<TableSchema> targetTables = new LinkedList<TableSchema>();
		Map<String, Integer> tableIndexMap = new TreeMap<String, Integer>();
		for (int i = 0; i < selectedTables.size(); i++) {
			SelectedTable st = selectedTables.get(i);
			TableSchema schema = TableSchema.load(st.getTableName());
			if (schema == null)	// 대상 테이블이 존재하지 않는 경우
				throw new SemanticError(String.format(Messages.SelectTableExistenceError, st.getTableName()));
			targetTables.add(schema);
			tableIndexMap.put(st.getTableName(), i);
		}

		// 출력 대상 칼럼들이 유효한지 검사하고, asterisk가 포함되어 있는지 확인한다
		boolean hasAsterisk = false;
		for (SelectedColumn sc : selectedColumns) {
			String columnName = sc.getColumnName();
			String tableName = sc.getTableName();

			// 출력 대상 칼럼 중에 asterisk가 포함되어 있으면, 모든 칼럼 유효성 검사 후 지정 칼럼을 모든 칼럼을 대상으로 출력하도록 변경한다.
			if (columnName.equals("*")) {
				hasAsterisk = true;
				continue;
			}

			// 출력 대상 칼럼들에 대한 유효성 검사
			if (tableName != null) {	// TABLENAME.COLUMNNAME
				if (!tableIndexMap.containsKey(tableName))		// 대상 테이블이 없는 경우
					throw new SemanticError(String.format(Messages.SelectTableExistenceError, tableName));
				TableSchema schema = targetTables.get(tableIndexMap.get(tableName));
				if (schema.getColumnIndex(columnName) == null)	// 대상 테이블에 해당 칼럼이 없는 경우
					throw new SemanticError(String.format(Messages.SelectColumnResolveError, columnName));
			} else {					// COLUMNNAME
				Integer columnIdx = null;
				for (TableSchema schema : targetTables) {
					Integer temp = schema.getColumnIndex(columnName);
					if (temp != null) {
						if (columnIdx != null)					// 테이블 명이 지정되지 않은 칼럼이 여러 테이블에서 등장할 경우
							throw new SemanticError(String.format(Messages.SelectColumnResolveError, columnName));
						else {
							// 지정된 칼럼의 테이블을 찾은 경우 지정된 칼럼에 테이블 정보를 업데이트한다.
							columnIdx = temp;
							sc.setTableName(schema.getName());
						}
					}
				}
				if (columnIdx == null)							// 지정된 칼럼을 어느 대상 테이블에서도 찾을 수 없는 경우
					throw new SemanticError(String.format(Messages.SelectColumnResolveError, columnName));
			}
		}

		// 출력 대상 칼럼이 asterisk이면, 대상 테이블의 모든 칼럼들을 출력한다
		// asterisk 외에 또 지정된 칼럼이 있더라도 칼럼에 대한 중복 출력 없이 asterisk만 있는 경우와 동일하게 처리한다
		if (hasAsterisk) {
			selectedColumns = new LinkedList<SelectedColumn>();
			for (TableSchema schema : targetTables) {
				for (ColumnDefinition cd : schema.getColumns()) {
					selectedColumns.add(new SelectedColumn(cd.getName(), schema.getName(), null));
				}
			}
		}

		// 파일로부터 모든 대상 테이블을 메모리로 로드
		FileManager fileMgr = FileManager.getInstance();
		List<Set<Row>> selectedTableRows = new LinkedList<Set<Row>>();
		for (SelectedTable st : selectedTables) {
			selectedTableRows.add(fileMgr.selectAll(st.getTableName()));
		}

		// 모든 테이블들에 대해 cartesian product 시행
		Set<List<Row>> resultSets = Sets.cartesianProduct(selectedTableRows);

		// Row 하나에 대해 먼저 Operator를 한 번 수행해보고 Operator가 유효한지 그렇지 않은지 확인한다
		if (operator != null && resultSets.size() != 0) {
			List<Row> firstRow = resultSets.iterator().next();
			// 만약 Operator가 유효하지않으면 여기서 에러가 발생할 것이고, SELECT 결과를 출력을 하지 않아도 된다.
			operator.eval(firstRow, targetTables, tableIndexMap);
		}

		// header 출력 (출력하려는 칼럼 명들)
		// 각 출력의 길이는 칼럼명 + 5으로 고정한다
		List<Integer> printColumnSizeList = new LinkedList<Integer>();
		boolean hasJoin = targetTables.size() > 1;	// JOIN 연산이 있을 경우 테이블명과 칼럼명을 모두 출력한다
		for (int line = 0; line < 3; line++) {
			if (line == 0 || line == 2) {
				System.out.print("+");
				for (SelectedColumn sc : selectedColumns) {
					int len;
					if (sc.getAlias() != null)
						len = sc.getAlias().length();
					else if (hasJoin)
						len = sc.getTableName().length() + 1 + sc.getColumnName().length();
					else
						len = sc.getColumnName().length();
					for (int i = 0; i < len + 5; i++) System.out.print("-");
					System.out.print("+");
				}
			} else if (line == 1) {
				System.out.print("|");
				for (SelectedColumn sc : selectedColumns) {
					// 해당 칼럼의 create table 시에 입력한 칼럼명 그대로 출력한다
					String columnName = targetTables.get(tableIndexMap.get(sc.getTableName())).getColumn(sc.getColumnName()).getName();
					// JOIN 연산의 결과를 출력할 경우엔 테이블명도 함께 출력한다
					if (hasJoin) columnName = sc.getTableName() + "." + columnName;
					// 칼럼 명에 대한 rename(as) 기능을 제공한다
					String printColumn = sc.getAlias() != null ? sc.getAlias() : columnName;
					System.out.print(printColumn);
					System.out.print("     |");
					printColumnSizeList.add(printColumn.length() + 5);
				}
			}
			System.out.println();
		}

		// cartesian product의 결과의 각 행에 Operator를 적용하고
		// Operator의 결과가 참인 경우에 한해 selectedColumns의 값을 출력
		// Operator가 없는 경우 즉 where 절이 없는 경우 모든 row를 출력한다
		for (List<Row> resultSet : resultSets) {
			if (operator == null || operator.eval(resultSet, targetTables, tableIndexMap) == Operator.EvalResult.TRUE) {
				System.out.print("|");
				for (int i = 0; i < selectedColumns.size(); i++) {
					Object value = getValue(selectedColumns.get(i), resultSet, targetTables, tableIndexMap);
					String printStr = null;
					if (value == null) {
						printStr = "null";
					} else if (value instanceof Integer) {
						printStr = value.toString();
					} else if (value instanceof String) {
						printStr = (String) value;
					} else if (value instanceof Date) {
						printStr = Constants.dateFormat.format((Date) value);
					} else {
						throw new SemanticError(String.format(Messages.UnexpectedError, "Unsupported Data Type"));
					}
					String formatStr = String.format("%%-%ds|", printColumnSizeList.get(i));
					System.out.print(String.format(formatStr, printStr));
				}
				System.out.println();
			}
		}

		// footer 출력
		System.out.print("+");
		for (SelectedColumn sc : selectedColumns) {
			int len = sc.getAlias() != null ? sc.getAlias().length() : sc.getColumnName().length();
			for (int i = 0; i < len + 5; i++) System.out.print("-");
			System.out.print("+");
		}
		System.out.println();
	}

	public static void insert(String tableName, List<String> columnNames, List<Object> values) throws SemanticError {
		// DB에 자신의 튜플를 삽입하는 메소드
		FileManager fileMgr = FileManager.getInstance();
		if (fileMgr == null) return;
		TableSchema schema = TableSchema.load(tableName);
		if (schema == null) throw new SemanticError(Messages.NoSuchTable);
		List<ColumnDefinition> columns = schema.getColumns();

		// 칼럼의 수와 값의 수가 일치하지 않는 경우
		if (columnNames != null && columnNames.size() != values.size())
			throw new SemanticError(Messages.InsertTypeMismatchError);
		else if (columnNames == null && columns.size() != values.size())
			throw new SemanticError(Messages.InsertTypeMismatchError);

		// insert 구문에 지정된 칼럼명들이 유효한지 확인
		if (columnNames != null) {
			List<String> alreadyChecked = new LinkedList<String>();
			for (String columnName : columnNames) {
				// 스키마에 정의되지 않은 칼럼이 포함되어 있는 경우
				if (schema.getColumn(columnName) == null)
					throw new SemanticError(String.format(Messages.InsertColumnExistenceError, columnName));
				// 지정된 칼럼명이 중복되는 경우
				if (alreadyChecked.contains(columnName))
					throw new SemanticError(String.format(Messages.InsertColumnDuplicatedError, columnName));
				alreadyChecked.add(columnName);
			}
		}

		Row row = new Row(columns.size());
		Row pkValues;

		// PK가 정의되지 않은 테이블의 경우 Row 자신이 키가 된다
		if (schema.getPrimaryKey() == null) pkValues = row;
		else pkValues = new Row(schema.getPrimaryKey().getColumns().size());

		// 입력하기 전에 점검해야 할 조건들을 전부 확인하고, 입력 칼럼에 해당하지 않는 값은 null로 채워준다
		for (int i = 0; i < columns.size(); i++) {
			// 테이블 스키마에 정의된 칼럼 순서대로 row를 짜맞춘다
			// 만약 insert 구문에 없는 칼럼의 경우 해당 값은 null로 채워준다
			ColumnDefinition cd = columns.get(i);
			Object value;

			// 테이블의 칼럼 정의에 있지만, select 구문에 지정되지 않은 칼럼이면 해당 value는 null로 입력한다
			// nullable 체크는 checkColumnValueConstraints 내부에서 한다
			if (columnNames != null) {
				int j = StringUtils.indexOfIgnoreCase(columnNames, cd.getName());
				if (j == -1) value = null;
				else value = values.get(j);
			} else {
				value = values.get(i);
			}

			// 만약 칼럼 데이터 타입이 char일 때, 선언된 길이보다 더 긴 값이 들어올 경우 뒷 부분을 자른다
			if (value != null && cd.getType().getClass() == CharDataType.class) {
				int limit = ((CharDataType) cd.getType()).getLimit();
				if (((String) value).length() > limit) value = ((String) value).substring(0, limit);
			}

			checkColumnValueConstraints(cd, value);

			row.add(value);
			if (cd.isPrimary()) {
				pkValues.add(value); // row에서 PK 에 해당하는 값들을 가져온다
				// 이런 일은 없겠지만 혹시 몰라서
				if (schema.getPrimaryKey() == null)
					throw new SemanticError(
							String.format(Messages.UnexpectedError, "Has PK with no PK definition in schema"));
			}
		}

		fileMgr.insertRow(schema.getName(), pkValues, row);
	}


	public static Pair<Integer, Integer> delete(String tableName, Operator operator) throws SemanticError {
		FileManager fileMgr = FileManager.getInstance();
		if (fileMgr == null) return null;

		int deletedCount = 0;
		int notDeletedDueToRI = 0;

		TableSchema schema = TableSchema.load(tableName);
		if (schema == null) throw new SemanticError(Messages.NoSuchTable);
		List<TableSchema> targetTables = new LinkedList<TableSchema>();
		targetTables.add(schema);
		Map<String, Integer> tableIndexMap = new TreeMap<String, Integer>();
		tableIndexMap.put(tableName, 0);
		for (Pair<Row, Row> kv : fileMgr.selectAllWithKey(tableName)) {
			Row key = kv.getLeft();
			Row row = kv.getRight();
			List<Row> resultSet = new LinkedList<Row>();
			resultSet.add(row);
			// where 절이 없으면 모든 절 삭제
			// where 절이 있으면 where 절을 만족하는 row만 삭제
			if (operator == null || operator.eval(resultSet, targetTables, tableIndexMap) == Operator.EvalResult.TRUE) {
				// 현재 삭제 하려는 Row를 삭제했을 경우 Referential Integrity Constraints를 위배하는 지 확인한다
				if (cascadeDeletion(schema, resultSet)) {
					fileMgr.deleteRow(tableName, key);
					deletedCount++;
				} else {
					notDeletedDueToRI++;
				}
			}
		}

		return Pair.of(deletedCount, notDeletedDueToRI);
	}

	private static boolean cascadeDeletion(TableSchema schema, List<Row> resultSet) throws SemanticError {
		// PK가 없다면 다른 곳에서 참조하고 있는 Row가 없다는 것을 의미하므로 cascade deletion 확인이 불필요
		if (schema.getPrimaryKey() == null) return true;

		// 참조하는 foreign key들이 모두 nullable 한지 확인
		for (String columnName : schema.getPrimaryKey().getColumns()) {
			ColumnDefinition cd = schema.getColumn(columnName);
			for (Pair<String, String> tc : cd.getReferencedTableColumns()) {
				String refedTableName = tc.getLeft();
				String refedColumnName = tc.getRight();
				TableSchema refedSchema = TableSchema.load(refedTableName);
				ColumnDefinition refedcd = refedSchema.getColumn(refedColumnName);
				if (refedcd.isNotNull()) return false;
			}
		}

		FileManager fileMgr = FileManager.getInstance();

		// 모두 nullable한 경우, 해당하는 값들을 null로 업데이트한다
		for (String columnName : schema.getPrimaryKey().getColumns()) {
			ColumnDefinition cd = schema.getColumn(columnName);
			Object deletingValue = resultSet.get(0).get(schema.getColumnIndex(columnName));
			for (Pair<String, String> tc : cd.getReferencedTableColumns()) {
				String refedTableName = tc.getLeft();
				String refedColumnName = tc.getRight();
				TableSchema refedSchema = TableSchema.load(refedTableName);
				Integer refedColumnIdx = refedSchema.getColumnIndex(refedColumnName);

				Set<Pair<Row, Row>> resultSets = fileMgr.selectAllWithKey(refedTableName);
				for (Pair<Row, Row> kv : resultSets) {
					Row key = kv.getLeft();
					Row row = kv.getRight();
					if (row.get(refedColumnIdx) == null) continue;
					if (row.get(refedColumnIdx).equals(deletingValue)) {
						row.set(refedColumnIdx, null);
						fileMgr.updateRow(refedTableName, key, row);
					}
				}
			}
		}

		return true;
	}

	private static void checkColumnValueConstraints(ColumnDefinition cd, Object value) throws SemanticError {
		// 칼럼 정의와 삽입하려는 값이 조건들에 잘 부합하는 지 확인한다

		if (value == null) {
			// 입력하려는 값이 null이고 해당 칼럼이 null을 허용하지 않는 경우
			if (cd.isNotNull()) throw new SemanticError(String.format(Messages.InsertColumnNonNullableError, cd.getName()));
		} else {
			// 칼럼이 외래키일 때, 해당 참조 테이블의 인스턴스에 현재 삽입하려는 값이 없을 경우
			if (cd.isForeign()) {
				FileManager fileMgr = FileManager.getInstance();
				boolean checkRI = false;
				for (Pair<String, String> tc : cd.getReferenceTableColumns()) {
					String tableName = tc.getLeft();
					String columnName = tc.getRight();
					List<Set<Row>> selectedTableRows = new LinkedList<Set<Row>>();
					selectedTableRows.add(fileMgr.selectAll(tableName));
					Set<List<Row>> resultSets = Sets.cartesianProduct(selectedTableRows);
					List<TableSchema> targetTables = new LinkedList<TableSchema>();
					targetTables.add(TableSchema.load(tableName));
					Map<String, Integer> tableIndexMap = new TreeMap<String, Integer>();
					tableIndexMap.put(tableName.toLowerCase(), 0);
					Operator operator = new CompareOperator(value, new SelectedColumn(columnName, tableName, null), "=");
					for (List<Row> resultSet : resultSets) {
						if (operator.eval(resultSet, targetTables, tableIndexMap) == Operator.EvalResult.TRUE) {
							checkRI = true;
							break;
						}
					}
					if (checkRI) break;
				}
				if (!checkRI) throw new SemanticError(Messages.InsertReferentialIntegrityError);
			}

			// 입력하려는  값의 타입이 칼럼 정의와 일치하지 않는 경우
			if (!checkTypeMatch(cd, value)) throw new SemanticError(Messages.InsertTypeMismatchError);

		}
	}

	private static boolean checkTypeMatch(ColumnDefinition cd, Object value) {
		// 칼럼 정의와 입력하려는 데이터의 타입이 일치하는 지 확인한다.
		if (cd.getType().getClass() == CharDataType.class) {
			return value.getClass() == String.class;
		} else if (cd.getType().getClass() == IntDataType.class) {
			return value.getClass() == Integer.class;
		} else if (cd.getType().getClass() == DateDataType.class) {
			return value.getClass() == Date.class;
		}

		return false;
	}

	public static Object getValue(SelectedColumn operand, List<Row> resultSet, List<TableSchema> targetTables,
			Map<String, Integer> tableIndexMap) throws SemanticError {
		// 현재 행(resultSet)에서 선택된 칼럼에 해당하는 값을 리턴한다
		String tableName = operand.getTableName();
		String columnName = operand.getColumnName();
		if (tableName != null) {
			Integer tableIdx = tableIndexMap.get(tableName);
			if (tableIdx == null)
				throw new SemanticError(Messages.WhereTableNotSpecified);
			TableSchema schema = targetTables.get(tableIdx);
			Integer columnIdx = schema.getColumnIndex(columnName);
			if (columnIdx == null)
				throw new SemanticError(Messages.WhereColumnNotExist);
			return resultSet.get(tableIdx).get(columnIdx);
		} else {
			Object value = null;
			for (int i = 0; i < targetTables.size(); i++) {
				Integer columnIdx = targetTables.get(i).getColumnIndex(columnName);
				if (columnIdx != null) {
					// 테이블명이 지정되지 않았는데, 여러 테이블에 동일한 칼럼명이 존재하는 경우
					if (value != null)
						throw new SemanticError(Messages.WhereAmbiguousReference);
					value = resultSet.get(i).get(columnIdx);
				}
			}
			if (value == null)
				throw new SemanticError(Messages.WhereColumnNotExist);
			return value;
		}
	}
}
