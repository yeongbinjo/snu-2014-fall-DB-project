package com.drunkenhaze.dbms.managers;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.IOException;
import java.io.ObjectInput;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.UnsupportedEncodingException;
import java.util.HashMap;
import java.util.HashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.apache.commons.lang3.tuple.Pair;

import com.drunkenhaze.dbms.components.Row;
import com.drunkenhaze.dbms.components.TableSchema;
import com.drunkenhaze.dbms.exceptions.SemanticError;
import com.drunkenhaze.dbms.main.Constants;
import com.drunkenhaze.dbms.main.Messages;
import com.sleepycat.bind.EntryBinding;
import com.sleepycat.bind.serial.SerialBinding;
import com.sleepycat.bind.serial.StoredClassCatalog;
import com.sleepycat.je.Cursor;
import com.sleepycat.je.Database;
import com.sleepycat.je.DatabaseConfig;
import com.sleepycat.je.DatabaseEntry;
import com.sleepycat.je.Environment;
import com.sleepycat.je.EnvironmentConfig;
import com.sleepycat.je.LockMode;
import com.sleepycat.je.OperationStatus;

public class FileManager {
	/*
	 * 데이터를 Berkeley DB를 이용하여 디스크에 저장하고 불러오는 역할을 수행하는 클래스(Data Access Class)입니다.
	 *
	 * 파일 I/O 작업은 모두 이 클래스를 거쳐야 하며 이 클래스의 인스턴스는 단 한 개만 존재합니다. (싱글톤 패턴)
	 * 멀티스레드 작업을 요구하지 않으므로 동시성 문제들은 처리하지 않았습니다.
	 */
	private static FileManager instance = null;
	private static String dbPath = null;

	private Environment env;
	private Database db;

	private Map<String, Pair<Environment, Database>> tableDBPool;	// 테이블 인스턴스를 저장하는 DB를 관리하는 풀

	public static void setDBPath(String _dbPath) {
		if (instance == null) FileManager.dbPath = _dbPath;
		else throw new IllegalStateException(Messages.CantChangeDBPath);
	}

	public static String getDBPath() {
		return FileManager.dbPath;
	}

	// Thread-Unsafe Singleton
	public static FileManager getInstance() {
		if (instance == null) {
			if (FileManager.dbPath == null) return null;
			instance = new FileManager();
		}

		return instance;
	}

	private FileManager() {
		init();
	}

	private void init() {
		Pair<Environment, Database> pair = openDB(new File(dbPath), Constants.DB_NAME);
		env = pair.getLeft();
		db = pair.getRight();

		tableDBPool = new HashMap<String, Pair<Environment, Database>>();
	}

	private Pair<Environment, Database> openDB(File dbPathFile, String dbName) {
		FileUtils.mkdirIfNotExists(dbPathFile);

		// Open Database Environment or if not, create one.
		EnvironmentConfig envConfig = new EnvironmentConfig();
		envConfig.setAllowCreate(true);
		Environment _env = new Environment(dbPathFile, envConfig);

		// Open Database or if not, create one
		DatabaseConfig dbConfig = new DatabaseConfig();
		dbConfig.setAllowCreate(true);
		dbConfig.setSortedDuplicates(false);
		Database _db = _env.openDatabase(null, dbName, dbConfig);

		return Pair.of(_env, _db);
	}

	private Database getDB(String tableName) {
		// Pool에서 DB 핸들을 가져온다
		// 현재 런타임에 한번도 접근한 적이 없는 테이블이라면, DB를 열고 Pool에 핸들을 등록한다
		tableName = tableName.toLowerCase();
		Pair<Environment, Database> pair = tableDBPool.get(tableName);
		if (pair == null) {
			pair = openDB(new File(dbPath, tableName), tableName);
			tableDBPool.put(tableName, pair);
		}
		return pair.getRight();
	}

	private void closeDB(String tableName) {
		// 테이블 스키마가 삭제될 때, 해당 테이블 인스턴스의 DB 핸들을 닫기 위한 메소드
		tableName = tableName.toLowerCase();
		Pair<Environment, Database> pair = tableDBPool.get(tableName);
		if (pair == null) return;
		pair.getRight().close();
		pair.getLeft().close();
		tableDBPool.remove(tableName);
	}

	private void closeHandles() {
		if (db != null) db.close();
		if (env != null) env.close();

		for (Pair<Environment, Database> pair : tableDBPool.values()) {
			pair.getRight().close();
			pair.getLeft().close();
		}
	}

	public void close() {
		closeHandles();

		instance = null;
	}

	public void blowup() {
		// 테스트 용 명령어(blowup)을 지원하기 위한 메소드
		// blowup; 을 입력하면 DB 데이터를 전부 초기화 시킨다.
		closeHandles();

		FileUtils.deleteFilesInDirectory(new File(FileManager.dbPath));
		init();
	}

	private DatabaseEntry getDatabaseEntryKeyFrom(String _key) throws SemanticError {
		// 전달받은 key에 해당하는 DatabaseEntry를 돌려주는 메소드
		// key는 반드시 UTF-8이며, case insensitive의 속성을 제공하기 위해 모든 key는 lower case로 관리된다.
		DatabaseEntry key = null;
		try {
			key = new DatabaseEntry(_key.toLowerCase().getBytes("UTF-8"));
		} catch (UnsupportedEncodingException e) {
			e.printStackTrace();
			throw new SemanticError(Messages.KeyEncodingError);
		}
		return key;
	}

	private DatabaseEntry getDatabaseEntryKeyFrom(Row pkValues) throws SemanticError {
		// 전달받은 PK들에 해당하는 DatabaseEntry를 돌려주는 메소드
		// PK들은 byte array로 변환된 뒤 결합하여 하나의 키를 이룬다
		DatabaseEntry key = null;
		ByteArrayOutputStream baos = new ByteArrayOutputStream();
		ObjectOutputStream oos = null;
		try {
			oos = new ObjectOutputStream(baos);
			oos.writeObject(pkValues);
			key = new DatabaseEntry(baos.toByteArray());
		} catch (IOException e) {
			e.printStackTrace();
		} finally {
			try {
				oos.close();
			} catch (IOException e) {
				e.printStackTrace();
			}
			try {
				baos.close();
			} catch (IOException e) {
				e.printStackTrace();
			}
		}

		return key;
	}

	public void putTableSchema(TableSchema tableSchema) throws SemanticError {
		// 테이블 스키마를 파일에 저장하는 메소드 (동일키 허용 금지)
		String tableName = tableSchema.getName().toLowerCase();

		// 같은 이름을 가진 테이블이 이미 존재하는 지 확인한 뒤
		if (getTableSchema(tableName) != null) throw new SemanticError(Messages.TableExistenceError);
		// 같은 이름을 가진 테이블이 없다면 updateTableSchema의 동작과 동일하다
		updateTableSchema(tableSchema);

		// 테이블 스키마를 저장 한 뒤 테이블 인스턴스 저장을 위한 Berkely DB를 생성하고 해당 DB의 핸들들을 런타임동안 Pool에서 관리한다
		tableDBPool.put(tableName, openDB(new File(dbPath, tableName), tableName));
	}

	public void updateTableSchema(TableSchema tableSchema) throws SemanticError {
		// 테이블 스키마를 파일에 저장하는 메소드 (동일키 허용 - 덮어쓰기)
		// Berkeley DB에서 제공하는 Bind 기능을 사용하여 TableSchema 객체를 그대로 직렬화하여 파일에 저장하였다.
		DatabaseEntry key = getDatabaseEntryKeyFrom(tableSchema.getName());
		DatabaseEntry data = new DatabaseEntry();

	    StoredClassCatalog classCatalog = new StoredClassCatalog(db);
	    EntryBinding<TableSchema> dataBinding = new SerialBinding<TableSchema>(classCatalog, TableSchema.class);

	    dataBinding.objectToEntry(tableSchema, data);	// 직렬화

	    db.put(null, key, data);
	}

	public TableSchema getTableSchema(String tableName) throws SemanticError {
		// 테이블 스키마를 파일로부터 불러오는 메소드
		// updateTableSchema에서 직렬화되어 저장된 TableSchema 객체를 그대로 파일로부터 가져온다.
		DatabaseEntry key = getDatabaseEntryKeyFrom(tableName);
		DatabaseEntry data = new DatabaseEntry();

	    StoredClassCatalog classCatalog = new StoredClassCatalog(db);
	    EntryBinding<TableSchema> dataBinding = new SerialBinding<TableSchema>(classCatalog, TableSchema.class);

	    OperationStatus status = db.get(null, key, data, LockMode.DEFAULT);
	    if (status == OperationStatus.NOTFOUND) return null;
	    TableSchema retrievedTable = dataBinding.entryToObject(data);	// 역직렬화

		return retrievedTable;
	}

	public void deleteTableSchema(TableSchema tableSchema) throws SemanticError {
		// 테이블 스키마를 파일로부터 제거하는 메소드
		// 반드시 이 메소드를 호출하기 전에 삭제하려는 테이블의 참조 정보를 완전히 정리해야 한다.
		String tableName = tableSchema.getName().toLowerCase();
		db.delete(null, getDatabaseEntryKeyFrom(tableName));

		// 테이블 인스턴스 저장을 위한 DB도 삭제한다
		closeDB(tableName);
		FileUtils.deleteDirectory(new File(dbPath, tableName));
	}

	public List<String> getTableNames() {
		// SHOW TABLE 기능을 위한 메소드
		// 파일에 저장되어 있는 모든 테이블의 이름을 리턴한다
		List<String> tableNames = new LinkedList<String>();

		Cursor cursor = db.openCursor(null, null);

		DatabaseEntry key = new DatabaseEntry();
		DatabaseEntry data = new DatabaseEntry();

	    StoredClassCatalog classCatalog = new StoredClassCatalog(db);
	    EntryBinding<TableSchema> dataBinding = new SerialBinding<TableSchema>(classCatalog, TableSchema.class);

		// DB를 전체 순회하면서 키 값(즉, 테이블 명)을 전부 가져온다.
		while (cursor.getNext(key, data, LockMode.DEFAULT) == OperationStatus.SUCCESS) {
			// DB에 테이블 명 말고 다른 객체가 포함될 수 있다.
			// 내가 직접 집어넣은 적은 없지만 아마 Bind API를 사용하면 자동으로 삽입되는 데이터들이 존재하는 듯 하다.
			// 때문에 deserialize 에 성공한 데이터의 키만 테이블 명으로 판단하도록 한다.
			// 다소 불필요한 동작을 요구하지만 테이블 명 외의 데이터들의 숫자가 그렇게 크지 않기 때문에
			// 이런 식으로 작성해도 성능 상 큰 무리가 없을 것으로 예상되며, 현재 어플리케이션의 용도 상 더 이상의 개선은 필요 없을 것으로 보인다.
			try {
				dataBinding.entryToObject(data);
				tableNames.add(new String(key.getData()));
			} catch (Exception e) {
				// do nothing
			}
		}
		cursor.close();

		return tableNames;
	}

	private Row selectRow(String tableName, Row pkValues) throws SemanticError {

		DatabaseEntry key = getDatabaseEntryKeyFrom(pkValues);
		DatabaseEntry data = new DatabaseEntry();

		// 해당 테이블에 대한 DB 핸들을 가져온다
		Database tableDB = getDB(tableName);

	    StoredClassCatalog classCatalog = new StoredClassCatalog(tableDB);
	    EntryBinding<Row> dataBinding = new SerialBinding<Row>(classCatalog, Row.class);

	    OperationStatus status = tableDB.get(null, key, data, LockMode.DEFAULT);
	    if (status == OperationStatus.NOTFOUND) return null;
	    Row row = dataBinding.entryToObject(data);	// 역직렬화

		return row;
	}

	public Set<Row> selectAll(String tableName) {
		// 해당 테이블 인스턴스에 저장되어 있는 모든 Row를 리턴한다
		Database curDb = getDB(tableName);
		Cursor cursor = curDb.openCursor(null, null);

		DatabaseEntry key = new DatabaseEntry();
		DatabaseEntry data = new DatabaseEntry();

	    StoredClassCatalog classCatalog = new StoredClassCatalog(curDb);
	    EntryBinding<Row> dataBinding = new SerialBinding<Row>(classCatalog, Row.class);

	    Set<Row> resultSet = new HashSet<Row>();

		// 테이블 DB를 전체 순회하면서 모든 Row 객체를 가져온다.
		while (cursor.getNext(key, data, LockMode.DEFAULT) == OperationStatus.SUCCESS) {
			try {
				Row curRow = dataBinding.entryToObject(data);
				resultSet.add(curRow);
			} catch (Exception e) {
				// do nothing
			}
		}
		cursor.close();

		return resultSet;
	}

	public Set<Pair<Row, Row>> selectAllWithKey(String tableName) {
		// 해당 테이블 인스턴스에 저장되어 있는 모든 Row와 Key를 리턴한다
		Database curDb = getDB(tableName);
		Cursor cursor = curDb.openCursor(null, null);

		DatabaseEntry key = new DatabaseEntry();
		DatabaseEntry data = new DatabaseEntry();

		StoredClassCatalog classCatalog = new StoredClassCatalog(curDb);
		EntryBinding<Row> dataBinding = new SerialBinding<Row>(classCatalog, Row.class);

		Set<Pair<Row, Row>> resultSet = new HashSet<Pair<Row, Row>>();

		// 테이블 DB를 전체 순회하면서 모든 Row 객체를 가져온다.
		while (cursor.getNext(key, data, LockMode.DEFAULT) == OperationStatus.SUCCESS) {
			try {
				Row curRow = dataBinding.entryToObject(data);
				ByteArrayInputStream bais = new ByteArrayInputStream(key.getData());
				ObjectInput oi = new ObjectInputStream(bais);
				Row curKey = (Row) oi.readObject();
				resultSet.add(Pair.of(curKey, curRow));
			} catch (Exception e) {
				// do nothing
			}
		}
		cursor.close();

		return resultSet;
	}

	public void insertRow(String tableName, Row pkValues, Row row) throws SemanticError {
		// INSERT 기능을 위한 메소드

		// 중복된 PK가 테이블 인스턴스에 존재하는 경우
		if (selectRow(tableName, pkValues) != null)
			throw new SemanticError(Messages.InsertDuplicatePrimaryKeyError);

		updateRow(tableName, pkValues, row);
	}

	public void updateRow(String tableName, Row pkValues, Row row) throws SemanticError {
		DatabaseEntry key = getDatabaseEntryKeyFrom(pkValues);
		DatabaseEntry data = new DatabaseEntry();

		// 해당 테이블에 대한 DB 핸들을 가져온다
		Database tableDB = getDB(tableName);

		StoredClassCatalog classCatalog = new StoredClassCatalog(tableDB);
		EntryBinding<Row> dataBinding = new SerialBinding<Row>(classCatalog, Row.class);

	    dataBinding.objectToEntry(row, data);	// 직렬화

	    tableDB.put(null, key, data);
	}

	public void deleteRow(String tableName, Row pkValues) throws SemanticError {
		// DELETE 기능을 위한 메소드

		// 반드시 이 메소드를 호출하기 전에 삭제하려는 행의 Referential Integrity Constraints를 위배하지 않는지 확인해야 한다
		getDB(tableName).delete(null, getDatabaseEntryKeyFrom(pkValues));
	}
}
