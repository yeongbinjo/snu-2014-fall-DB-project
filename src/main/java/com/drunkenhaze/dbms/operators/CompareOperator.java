package com.drunkenhaze.dbms.operators;

import java.util.Date;
import java.util.List;
import java.util.Map;

import com.drunkenhaze.dbms.components.Row;
import com.drunkenhaze.dbms.components.SelectedColumn;
import com.drunkenhaze.dbms.components.TableSchema;
import com.drunkenhaze.dbms.exceptions.SemanticError;
import com.drunkenhaze.dbms.main.Messages;
import com.drunkenhaze.dbms.managers.DMLManager;

public class CompareOperator implements Operator {

	private Object operand1;
	private Object operand2;
	private String compOperator;

	public CompareOperator(Object operand1, Object operand2, String compOperator) {
		this.operand1 = operand1;
		this.operand2 = operand2;
		this.compOperator = compOperator;
	}

	@Override
	public EvalResult eval(List<Row> resultSet, List<TableSchema> targetTables,
			Map<String, Integer> tableIndexMap) throws SemanticError {
		Object value1, value2;
		if (operand1 instanceof SelectedColumn)
			value1 = DMLManager.getValue((SelectedColumn) operand1, resultSet, targetTables, tableIndexMap);
		else value1 = operand1;
		if (operand2 instanceof SelectedColumn)
			value2 = DMLManager.getValue((SelectedColumn) operand2, resultSet, targetTables, tableIndexMap);
		else value2 = operand2;

		// 비교 연산에서는 둘 중 하나가 null이면 결과는 UNKNOWN이다
		if (value1 == null || value2 == null) return EvalResult.UNKNOWN;

		// 비교 대상의 타입은 서로 같아야 한다
		if (value1 != null && value2 != null && value1.getClass() != value2.getClass())
			throw new SemanticError(Messages.WhereIncomparableError);

		int compareResult;
		if (value1 instanceof Integer) {
			compareResult = ((Integer) value1).compareTo((Integer) value2);
		} else if (value1 instanceof String) {
			compareResult = ((String) value1).compareTo((String) value2);
		} else if (value1 instanceof Date) {
			compareResult = ((Date) value1).compareTo((Date) value2);
		} else {
			throw new SemanticError(String.format(Messages.UnexpectedError, "Unsupported Comp Operand"));
		}

		switch (compOperator) {
		case "<":
			if (compareResult < 0) return EvalResult.TRUE;
			else return EvalResult.FALSE;
		case ">":
			if (compareResult > 0) return EvalResult.TRUE;
			else return EvalResult.FALSE;
		case "=":
			if (compareResult == 0) return EvalResult.TRUE;
			else return EvalResult.FALSE;
		case ">=":
			if (compareResult >= 0) return EvalResult.TRUE;
			else return EvalResult.FALSE;
		case "<=":
			if (compareResult <= 0) return EvalResult.TRUE;
			else return EvalResult.FALSE;
		case "!=":
			if (compareResult != 0) return EvalResult.TRUE;
			else return EvalResult.FALSE;
		default:
			throw new SemanticError(String.format(Messages.UnexpectedError, "Unsupported Comp Operator"));
		}
	}

}
