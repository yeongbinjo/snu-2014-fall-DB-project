package com.drunkenhaze.dbms.main;

import java.text.DateFormat;
import java.text.SimpleDateFormat;

public class Constants {
	public static final String DB_PATH = "src/main/resources/db/";
	public static final String DB_NAME = "drunkenhazeDatabase";
	public static final String TEST_DB_PATH = "src/main/resources/db_test/";

	public static final int DESC_PADDING_MAX_LENGTH = 80;

	public static final DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");

	static {
		dateFormat.setLenient(false);
	}
}
