package com.drunkenhaze.dbms.main;

public class Messages {
	// MESSAGES
	public static final String Pr1_1TemporarySuccess = "'%s' is requested";
	public static final String SyntaxError = "Syntax error";
	public static final String CreateTableSuccess = "'%s' table is created";
	public static final String DuplicateColumnDefError = "Create table has failed: column definition is duplicated";
	public static final String DuplicatePrimaryKeyDefError = "Create table has failed: primary key definition is duplicated";
	public static final String ReferenceTypeError = "Create table has failed: foreign key references wrong type";
	public static final String ReferenceNonPrimaryKeyError = "Create table has failed: foreign key references non primary key column";
	public static final String ReferenceColumnExistenceError = "Create table has failed: foreign key references non existing column";
	public static final String ReferenceTableExistenceError = "Create table has failed: foreign key references non existing table";
	public static final String NonExistingColumnDefError = "Create table has failed: '%s' does not exists in column definition";
	public static final String TableExistenceError = "Create table has failed: table with the same name already exists";
	public static final String InsertResult = "The row is inserted";
	public static final String InsertDuplicatePrimaryKeyError = "Insertion has failed: Primary key duplication";
	public static final String InsertReferentialIntegrityError = "Insertion has failed: Referential integrity violation";
	public static final String InsertTypeMismatchError = "Insertion has failed: Types are not matched";
	public static final String InsertColumnExistenceError = "Insertion has failed: '%s' does not exist";
	public static final String InsertColumnNonNullableError = "Insertion has failed: '%s' is not nullable";
	public static final String DeleteResult = "%d row(s) are deleted";
	public static final String DeleteReferentialIntegrityPassed = "%d row(s) are not deleted due to referential integrity";
	public static final String SelectTableExistenceError = "Selection has failed: '%s' does not exist";
	public static final String SelectColumnResolveError = "Selection has failed: fail to resolve '%s'";
	public static final String SelectNonComparableTypesError = "Where condition error: types are not matched in comparison";
	public static final String SelectDuplicateAliasingError = "Alias is duplicated";
	public static final String DropSuccess = "'%s' table is dropped";
	public static final String DropReferencedTableError = "Drop table has failed: '%s' is referenced by other table";
	public static final String ShowTablesNoTable = "There is no table";
	public static final String NoSuchTable = "No such table";
	public static final String InvalidDateRangeError = "Date value is in incorrect range";
	public static final String CharLengthError = "Char length should be > 0";
	public static final String WhereIncomparableError = "Where clause try to compare incomparable values";
	public static final String WhereTableNotSpecified = "Where clause try to reference tables which are not specified";
	public static final String WhereColumnNotExist = "Where clause try to reference non existing column";
	public static final String WhereAmbiguousReference = "Where clause contains ambiguous reference";

	// CUSTOM MESSAGES
	public static final String KeyEncodingError = "Key must be UTF-8 encoding";
	public static final String SpaceBetweenTableNameColumnName = "Blanks or Newline Characters are not allowed between TABLENAME and COLUMNNAME";
	public static final String SpaceBetweenNotNull = "Need space between 'not null' and 'data type'";
	public static final String SpaceBetweenReferences = "Need space between 'references' and 'column name list'";
	public static final String SetDBPathBeforeInitialize = "Please set db path for file manager before call getInstance()";
	public static final String CantChangeDBPath = "Can't change db path, when FileManager instance already initialized";

	public static final String InsertColumnDuplicatedError = "Insertion has failed: '%s' is duplicated";

	public static final String UnexpectedError = "UnexpectedError: %s";
}
