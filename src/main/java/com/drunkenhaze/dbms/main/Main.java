package com.drunkenhaze.dbms.main;

import com.drunkenhaze.dbms.exceptions.SemanticError;
import com.drunkenhaze.dbms.managers.FileManager;


public class Main {
	public static void main(String[] args) {
		FileManager.setDBPath(Constants.DB_PATH);
		FileManager fileMgr = FileManager.getInstance();

		SQLParser parser = new SQLParser(System.in);
		parser.init();
		parser.setFileManager(fileMgr);

		/*
		 * START PARSER
		 */
		boolean keepGoing = true;
		while (keepGoing) {
			try {
				// 입력 스트림이 비어있을 경우에만 입력을 대기하는 쉘을 출력한다
				if (parser.isStreamEmpty()) parser.printShell();

				keepGoing = parser.parse();
			} catch (ParseException e) {
				// 파싱 과정에서의 에러 즉 syntax error의 경우
				// 적절한 메세지를 출력한 뒤,
				// 현재까지 처리된 입력 이후의 구문은 버리고 다음 입력을 대기한다.
				parser.printShell("Syntax error");
				parser.ReInit(System.in);
			} catch (SemanticError e) {
				parser.printShell(e.getMessage());
				// 현재 구문이 완전히 파싱 종료되었을 때 SemanticError가 발생했다면 다음 세미콜론까지 스킵할 필요가 없음
				if (parser.getToken(0).image != ";") {
					parser.errorSkipTo(SQLParser.SEMICOLON);
				}
			} catch (Exception e) {
				e.printStackTrace();
			}
		}

		fileMgr.close();
	}
}
