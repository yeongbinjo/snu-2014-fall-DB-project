package com.drunkenhaze.dbms.components;

import java.io.Serializable;
import java.util.List;

public class ForeignKey implements Serializable {
	private static final long serialVersionUID = 7045385866352082791L;
	private List<String> columnNames;
	private String refTableName;
	private List<String> refColumnNames;

	public ForeignKey(List<String> _columnNames,
			String _refTableName,
			List<String> _refColumnNames){
		this.columnNames = _columnNames;
		this.refTableName = _refTableName;
		this.refColumnNames = _refColumnNames;
	}

	public String getRefTableName() {
		return refTableName;
	}

	public List<String> getColumnNames() {
		return columnNames;
	}

	public List<String> getRefColumnNames() {
		return refColumnNames;
	}
}
