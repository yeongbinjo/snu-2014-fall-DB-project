package com.drunkenhaze.dbms.components;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

public class Row implements Serializable {
	// 테이블 인스턴스의 각 행의 정보를 담기 위한 컨테이너 클래스

	private static final long serialVersionUID = 1288934257710866566L;

	private List<Object> values;

	public Row(int size) {
		values = new ArrayList<Object>(size);
	}

	public void add(Object o) {
		values.add(o);
	}

	public Object get(int idx) {
		return values.get(idx);
	}

	public void set(int idx, Object o) {
		values.set(idx, o);
	}

	@Override
	public String toString() {
		return "Row [values=" + values + "]";
	}
}
