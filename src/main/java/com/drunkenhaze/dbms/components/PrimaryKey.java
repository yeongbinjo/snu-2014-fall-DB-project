package com.drunkenhaze.dbms.components;

import java.io.Serializable;
import java.util.List;

public class PrimaryKey implements Serializable {
	private static final long serialVersionUID = 1384703472154205196L;
	private List<String> columnNames;

	public PrimaryKey(TableSchema tableSchema, List<String> _columnNames) {
		columnNames = _columnNames;
	}

	public List<String> getColumns() {
		return columnNames;
	}

	@Override
	public String toString() {
		return "PrimaryKey [columnNames=" + columnNames + "]";
	}
}
