package com.drunkenhaze.dbms.components;

public class SelectedTable {
	/*
	 * select 구문의 from 절에서 선택된 테이블에 대한 정보를 담는 클래스
	 *
	 * - 테이블명
	 * - alias (옵션)
	 * case insensitive 속성을 유지하기 위해 선택된 테이블명은 전부 lowercase로 저장된다.
	 */
	private String tableName;
	private String alias;

	public SelectedTable() {
		tableName = null;
		alias = null;
	}

	public String getTableName() {
		return tableName;
	}

	public void setTableName(String tableName) {
		this.tableName = tableName == null ? null : tableName.toLowerCase();
	}

	public String getAlias() {
		return alias;
	}

	public void setAlias(String alias) {
		this.alias = alias;
	}


}
