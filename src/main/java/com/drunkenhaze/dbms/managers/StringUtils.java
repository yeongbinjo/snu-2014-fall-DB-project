package com.drunkenhaze.dbms.managers;

import java.util.List;

public class StringUtils {
	public static int indexOfIgnoreCase(List<String> list, String str) {
		int i = 0;
		for (String item : list) {
			if (item.equalsIgnoreCase(str)) return i;
			i++;
		}
		return -1;
	}
}
