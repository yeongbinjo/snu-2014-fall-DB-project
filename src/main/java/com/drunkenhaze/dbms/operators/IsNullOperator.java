package com.drunkenhaze.dbms.operators;

import java.util.List;
import java.util.Map;

import com.drunkenhaze.dbms.components.ColumnDefinition;
import com.drunkenhaze.dbms.components.Row;
import com.drunkenhaze.dbms.components.SelectedColumn;
import com.drunkenhaze.dbms.components.TableSchema;
import com.drunkenhaze.dbms.exceptions.SemanticError;
import com.drunkenhaze.dbms.main.Messages;

public class IsNullOperator implements Operator {

	private SelectedColumn operand;
	private boolean checkIsNull;

	public IsNullOperator(SelectedColumn operand, boolean checkIsNull) {
		this.operand = operand;
		this.checkIsNull = checkIsNull;
	}

	@Override
	public EvalResult eval(List<Row> resultSet, List<TableSchema> targetTables,
			Map<String, Integer> tableIndexMap) throws SemanticError {
		ColumnDefinition foundCd = null;
		// 현재 행에서 SelectedColumn에 해당하는 값을 가져온다
		if (operand.getTableName() != null) {
			Integer tableIdx = tableIndexMap.get(operand.getTableName());
			if (tableIdx == null)
				throw new SemanticError(Messages.WhereTableNotSpecified);
			for (ColumnDefinition cd : targetTables.get(tableIdx).getColumns()) {
				if (cd.getName().equalsIgnoreCase(operand.getColumnName())) {
					foundCd = cd;
					break;
				}
			}
		} else {
			for (TableSchema schema : targetTables) {
				for (ColumnDefinition cd : schema.getColumns()) {
					if (cd.getName().equalsIgnoreCase(operand.getColumnName())) {
						if (foundCd != null)	// 지정된 테이블이 없는데, 같은 칼럼이 여러 테이블에 출현하는 경우
							throw new SemanticError(Messages.WhereAmbiguousReference);
						foundCd = cd;
						break;
					}
				}
			}
		}
		if (foundCd == null)
			throw new SemanticError(Messages.WhereColumnNotExist);

		if (checkIsNull) {
			if (foundCd.isNotNull()) return EvalResult.FALSE;
			else return EvalResult.TRUE;
		} else {
			if (foundCd.isNotNull()) return EvalResult.TRUE;
			else return EvalResult.FALSE;
		}
	}
}
