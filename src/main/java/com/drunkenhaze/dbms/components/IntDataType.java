package com.drunkenhaze.dbms.components;

import java.io.Serializable;

public class IntDataType implements ColumnDataType, Serializable {
	private static final long serialVersionUID = 3668511540192186171L;

	@Override
	public String toString() {
		return "int";
	}

	@Override
	public boolean equals(Object other) {
		if (other == null) return false;
		if (other == this) return true;
		if (!(other instanceof IntDataType)) return false;
		return true;
	}
}
