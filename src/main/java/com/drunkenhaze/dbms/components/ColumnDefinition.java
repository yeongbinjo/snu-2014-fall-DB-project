package com.drunkenhaze.dbms.components;

import java.io.Serializable;
import java.util.LinkedList;
import java.util.List;

import org.apache.commons.lang3.tuple.Pair;

public class ColumnDefinition implements Serializable {
	private static final long serialVersionUID = 3584701221252917054L;

	/*
	 * 칼럼 정의 정보를 갖고 있는 클래스입니다.
	 *
	 * 칼럼명, 칼럼타입, 칼럼속성의 정보를 포함하고 있습니다.
	 */
	private enum KeyType { NONE, PRIMARY, FOREIGN, PRIFOR };

	private String name;
	private ColumnDataType type;
	private boolean notNull;
	private KeyType keyType;

	private List<Pair<String, String>> referenceTableColumns;
	private List<Pair<String, String>> referencedTableColumns;

	public ColumnDefinition(String name, ColumnDataType type) {
		this.name = name;
		this.type = type;
		notNull = false;
		keyType = KeyType.NONE;
		referenceTableColumns = new LinkedList<Pair<String, String>>();
		referencedTableColumns = new LinkedList<Pair<String, String>>();
	}

	public void setNotNull() {
		notNull = true;
	}

	public boolean isNotNull() {
		return notNull;
	}

	public String getName() {
		return name;
	}

	public ColumnDataType getType() {
		return type;
	}

	public void setPrimary() {
		if (keyType == KeyType.FOREIGN) keyType = KeyType.PRIFOR;
		else if (keyType == KeyType.NONE) keyType = KeyType.PRIMARY;
	}

	public boolean isPrimary() {
		return keyType == KeyType.PRIMARY || keyType == KeyType.PRIFOR;
	}

	public void setForeign() {
		if (keyType == KeyType.PRIMARY) keyType = KeyType.PRIFOR;
		else if (keyType == KeyType.NONE) keyType = KeyType.FOREIGN;
	}

	public boolean isForeign() {
		return keyType == KeyType.FOREIGN || keyType == KeyType.PRIFOR;
	}

	public List<Pair<String, String>> getReferenceTableColumns() {
		return referenceTableColumns;
	}

	public void addReferenceTableColumn(String tableName, String columnName) {
		referenceTableColumns.add(Pair.of(tableName, columnName));
	}

	public List<Pair<String, String>> getReferencedTableColumns() {
		return referencedTableColumns;
	}

	public void addReferencedTableColumn(String tableName, String columnName) {
		referencedTableColumns.add(Pair.of(tableName, columnName));
	}

	public void removeReferencedTableColumn(String tableName, String columnName) {
		referencedTableColumns.remove(Pair.of(tableName, columnName));
	}

	@Override
	public String toString() {
		StringBuilder sb = new StringBuilder();
		sb.append(String.format("%-30s %-20s %-20s ",
				name,
				type.toString(),
				notNull ? "N" : "Y"));
		if (isPrimary() && isForeign()) {
			sb.append("PRI/FOR");
		} else if (isPrimary()) {
			sb.append("PRI");
		} else if (isForeign()) {
			sb.append("FOR");
		}
		return sb.toString();
	}
}
