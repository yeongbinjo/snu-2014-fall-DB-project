package com.drunkenhaze.dbms.components;

import java.io.Serializable;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.TreeMap;

import com.drunkenhaze.dbms.exceptions.SemanticError;
import com.drunkenhaze.dbms.main.Constants;
import com.drunkenhaze.dbms.main.Messages;
import com.drunkenhaze.dbms.managers.FileManager;

public class TableSchema implements Serializable {
	private static final long serialVersionUID = 3446546562157072763L;
	/*
	 * 테이블 스키마를 나타내는 클래스입니다.
	 *
	 * This class does not mean table instance. It mean table schema.
	 * 테이블명, 칼럼 정의 리스트, Primary Key, Foreign Key에 대한 정보를 포함하고 있습니다.
	 */
	private String name;						// 테이블 명
	private List<ColumnDefinition> columns;		// 칼럼 정의
	private PrimaryKey pk;						// PK
	private List<ForeignKey> fks;				// FK들

	private List<String> referenced;			// 이 테이블을 참조하고 있는 테이블의 리스트
	private Map<String, Integer> columnIndexMap;// 각 칼럼이 몇 번쨰 칼럼인지 인덱스 정보를 갖고 있는 맵

	public TableSchema(String name) {
		this.name = name;

		pk = null;
		columns = new LinkedList<ColumnDefinition>();
		fks = new LinkedList<ForeignKey>();

		referenced = new LinkedList<String>();
		columnIndexMap = new TreeMap<String, Integer>();
	}

	public void addColumnDefinition(ColumnDefinition _cd) throws SemanticError {
		// 추가하려는 칼럼 정의와 같은 이름의 칼럼 정의가 이미 존재하는 지 확인하고, 존재한다면 예외를 발생시킨다.
		for (ColumnDefinition cd : columns) {
			if (cd.getName().toLowerCase().equals(_cd.getName().toLowerCase())) {
				throw new SemanticError(Messages.DuplicateColumnDefError);
			}
		}

		// 유효성 검사가 완료되면 현재 테이블의 칼럼 정의를 추가한다.
		columns.add(_cd);

		// 칼럼 정의를 추가함과 동시에 칼럼명과 저장 순서를 매핑하는 인덱스 맵을 구성한다
		columnIndexMap.put(_cd.getName().toLowerCase(), columns.size() - 1);
	}

	public void setPrimaryKey(PrimaryKey _pk) throws SemanticError {
		// PK가 이미 정의되어 있으면, 하나의 create table 구문에 primary key constraint 가 두 개 이상 존재하는 것이므로 예외를 발생시킨다.
		if (pk != null) {
			throw new SemanticError(Messages.DuplicatePrimaryKeyDefError);
		}
		this.pk = _pk;
	}

	public void addForeignKey(ForeignKey _fk) throws SemanticError {
		fks.add(_fk);
	}

	public String getName() {
		return name;
	}

	public List<ColumnDefinition> getColumns() {
		return columns;
	}

	public ColumnDefinition getColumn(String columnName) {
		// 칼럼명에 해당하는 실제 칼럼 정의를 받아온다
		// 이 메소드는 FK 선언 시 각 칼럼들의 데이터 타입이 서로 일치하는 지 확인하기 위해서 구현되었다
		for (ColumnDefinition cd : columns) {
			if (cd.getName().toLowerCase().equals(columnName.toLowerCase())) return cd;
		}
		return null;
	}

	public void addReferencedTable(String tableName) {
		// 자신을 참조하고 있는 테이블의 정보를 추가한다
		referenced.add(tableName);
	}

	public void removeReferencedTable(String tableName) {
		// 삭제될 테이블에 대한 참조 정보를 삭제한다
		referenced.remove(tableName);
	}

	public boolean isRemovable() {
		// 현재 테이블을 참조하고 있는 다른 테이블이 있는지 확인 한 뒤 없으면 true를 리턴한다.
		if (referenced.size() == 0 ) return true;
		else return false;
	}

	public Integer getColumnIndex(String columnName) {
		return columnIndexMap.get(columnName.toLowerCase());
	}

	public PrimaryKey getPrimaryKey() {
		return pk;
	}

	public void verify() throws SemanticError {
		FileManager fileMgr = FileManager.getInstance();
		if (fileMgr == null) return;

		// PK 유효성 체크
		// PK를 구성하는 칼럼들이 이미 정의되어있는 칼럼인지 확인하고, 만약 정의되지 않은 칼럼이 PK로 선언되어 있으면 예외를 발생시킨다.
		if (pk != null) {
			for (String pkColumnName : pk.getColumns()) {
				boolean hasColumn = false;		// 정의된 칼럼이 존재하는가?
				for (ColumnDefinition column : columns) {
					if (column.getName().toLowerCase().equals(pkColumnName.toLowerCase())) {
						hasColumn = true;		// 정의된 칼럼이 존재한다
						column.setPrimary();	// 해당 칼럼 정의에 PK 정보를 추가하고
						column.setNotNull();	// 해당 칼럼은 반드시 NOT NULL이어야 한다.
						break;
					}
				}
				// 정의된 칼럼이 존재하지 않는다면 예외 발생
				if (!hasColumn) throw new SemanticError(String.format(Messages.NonExistingColumnDefError, pkColumnName));
			}
		}

		// FK 유효성 체크
		// 모든 FK에 대해 Referential Integrity Constraints를 확인한다
		for (ForeignKey _fk : fks) {
			TableSchema refTable = fileMgr.getTableSchema(_fk.getRefTableName());
			if (refTable == null)
				throw new SemanticError(Messages.ReferenceTableExistenceError);	// 참조 테이블이 존재하지 않을 경우

			List<String> columnNames = _fk.getColumnNames();
			List<String> refColumnNames = _fk.getRefColumnNames();
			if (columnNames.size() != refColumnNames.size())
				throw new SemanticError(Messages.ReferenceTypeError);	// 칼럼 개수가 서로 일치하지 않는 경우

			for (int i = 0; i < columnNames.size(); i++) {
				ColumnDefinition cd = getColumn(columnNames.get(i));
				ColumnDefinition rcd = refTable.getColumn(refColumnNames.get(i));

				if (cd == null)								// FK로 선언하려는 칼럼이 정의되지 않은 경우
					throw new SemanticError(String.format(Messages.NonExistingColumnDefError, columnNames.get(i)));
				if (rcd == null)							// 참조 칼럼이 없는 경우
					throw new SemanticError(Messages.ReferenceColumnExistenceError);
				if (!rcd.isPrimary())						// 참조 칼럼이 PK가 아닐 경우
					throw new SemanticError(Messages.ReferenceNonPrimaryKeyError);
				if (!cd.getType().equals(rcd.getType()))	// 참조 칼럼과 데이터 타입이 일치하지 않는 경우
					throw new SemanticError(Messages.ReferenceTypeError);

				cd.setForeign();
				cd.addReferenceTableColumn(refTable.getName(), rcd.getName());
				rcd.addReferencedTableColumn(getName(), cd.getName());
			}

			// 유효한 FK임이 확인되면 참조 테이블에 참조 정보를 갱신한 뒤 DB에 해당 정보를 저장한다.
			refTable.addReferencedTable(getName());
			refTable.update();
		}
	}

	public void save() throws SemanticError {
		// DB에 자신의 정보를 저장하는 메소드
		FileManager fileMgr = FileManager.getInstance();
		if (fileMgr == null) return;

		fileMgr.putTableSchema(this);
	}

	public void update() throws SemanticError {
		// DB에 자신의 정보를 업데이트하는 메소드
		FileManager fileMgr = FileManager.getInstance();
		if (fileMgr == null) return;

		fileMgr.updateTableSchema(this);
	}

	public void drop() throws SemanticError {
		// DB에 자신의 정보를 삭제하는 메소드
		// Referential Integrity Constraint 를 준수하기 위해, 테이블을 삭제하기 전에 먼저 테이블의 참조 정보를 전부 지운 뒤에 그 자신을 삭제한다
		FileManager fileMgr = FileManager.getInstance();
		if (fileMgr == null) return;

		if (isRemovable()) {										// 삭제하려는 테이블을 참조하고 있는 테이블이 있으면 삭제할 수 없다
			for (ForeignKey fk : fks) {								// 삭제하려는 테이블이 참조하고 있는 모든 테이블에 대해서
				TableSchema refTable = fileMgr.getTableSchema(fk.getRefTableName());

				int i = 0;											// 각 칼럼 별 참조 정보와
				for (i = 0; i < fk.getRefColumnNames().size(); i++) {
					ColumnDefinition rcd = refTable.getColumn(fk.getColumnNames().get(i));
					rcd.removeReferencedTableColumn(getName(), getColumn(fk.getColumnNames().get(i)).getName());
				}

				refTable.removeReferencedTable(getName());			// 테이블 참조 정보를 삭제한 뒤
				refTable.update();									// 갱신된 정보를 파일에 쓴다
			}
			fileMgr.deleteTableSchema(this);						// 모든 참조 정보가 삭제되었으면, 현재 테이블을 삭제한다
		} else {
			throw new SemanticError(String.format(Messages.DropReferencedTableError, getName()));
		}
	}


	@Override
	public String toString() {
		// toString()의 출력값이 DESC 구문의 출력값이 되도록한다.
		StringBuilder sb = new StringBuilder();
		// 패딩 길이는 총 80 (30 + 1 + 20 + 1 + 20 + 1 + 7)
		// 칼럼명의 길이가 30보다 작게 입력될 것이라고 가정함 (길이가 30을 넘는 칼럼명을 허용하지 않는 것은 아니지만, 만약 입력될 경우 출력은 예쁘게 안됨)
		for (int i = 0; i < Constants.DESC_PADDING_MAX_LENGTH; i++) sb.append("-");
		sb.append("\ntable_name [" + name + "]\n");
		sb.append(String.format("%-30s %-20s %-20s %s\n", "column_name", "type", "null", "key"));
		for (ColumnDefinition cd : columns) {
			sb.append(cd.toString() + "\n");
		}
		for (int i = 0; i < Constants.DESC_PADDING_MAX_LENGTH; i++) sb.append("-");
		return sb.toString();
	}

	public static TableSchema load(String tableName) throws SemanticError {
		// DB로부터 TableSchema를 읽어오는 메소드
		FileManager fileMgr = FileManager.getInstance();
		if (fileMgr == null) return null;

		return fileMgr.getTableSchema(tableName);
	}
}
