/* Generated By:JavaCC: Do not edit this line. SQLParserConstants.java */
package com.drunkenhaze.dbms.main;


/**
 * Token literal values and constants.
 * Generated by org.javacc.parser.OtherFilesGen#start()
 */
public interface SQLParserConstants {

  /** End of File. */
  int EOF = 0;
  /** RegularExpression Id. */
  int SEMICOLON = 6;
  /** RegularExpression Id. */
  int PERIOD = 7;
  /** RegularExpression Id. */
  int ASTERISK = 8;
  /** RegularExpression Id. */
  int EXIT = 9;
  /** RegularExpression Id. */
  int CREATE = 10;
  /** RegularExpression Id. */
  int DROP = 11;
  /** RegularExpression Id. */
  int TABLE = 12;
  /** RegularExpression Id. */
  int TABLES = 13;
  /** RegularExpression Id. */
  int SHOW = 14;
  /** RegularExpression Id. */
  int DESC = 15;
  /** RegularExpression Id. */
  int SELECT = 16;
  /** RegularExpression Id. */
  int INSERT = 17;
  /** RegularExpression Id. */
  int INTO = 18;
  /** RegularExpression Id. */
  int FROM = 19;
  /** RegularExpression Id. */
  int DELETE = 20;
  /** RegularExpression Id. */
  int PRIMARY = 21;
  /** RegularExpression Id. */
  int FOREIGN = 22;
  /** RegularExpression Id. */
  int KEY = 23;
  /** RegularExpression Id. */
  int REFERENCES = 24;
  /** RegularExpression Id. */
  int IS = 25;
  /** RegularExpression Id. */
  int NOT = 26;
  /** RegularExpression Id. */
  int NULL = 27;
  /** RegularExpression Id. */
  int VALUES = 28;
  /** RegularExpression Id. */
  int AS = 29;
  /** RegularExpression Id. */
  int OR = 30;
  /** RegularExpression Id. */
  int AND = 31;
  /** RegularExpression Id. */
  int WHERE = 32;
  /** RegularExpression Id. */
  int INT = 33;
  /** RegularExpression Id. */
  int CHAR = 34;
  /** RegularExpression Id. */
  int DATE = 35;
  /** RegularExpression Id. */
  int BLOWUP = 36;
  /** RegularExpression Id. */
  int _ALPHABET = 37;
  /** RegularExpression Id. */
  int _QUOTE = 38;
  /** RegularExpression Id. */
  int _NONQUOTECHAR = 39;
  /** RegularExpression Id. */
  int _SIGN = 40;
  /** RegularExpression Id. */
  int _DIGIT = 41;
  /** RegularExpression Id. */
  int _INTVALUE = 42;
  /** RegularExpression Id. */
  int _CHARSTRING = 43;
  /** RegularExpression Id. */
  int _DATEVALUE = 44;
  /** RegularExpression Id. */
  int _UNDERSCORE = 45;
  /** RegularExpression Id. */
  int _LEFTPAREN = 46;
  /** RegularExpression Id. */
  int _RIGHTPAREN = 47;
  /** RegularExpression Id. */
  int INTVALUE = 48;
  /** RegularExpression Id. */
  int CHARSTRING = 49;
  /** RegularExpression Id. */
  int DATEVALUE = 50;
  /** RegularExpression Id. */
  int LEFTPAREN = 51;
  /** RegularExpression Id. */
  int RIGHTPAREN = 52;
  /** RegularExpression Id. */
  int COMMA = 53;
  /** RegularExpression Id. */
  int COMPOP = 54;
  /** RegularExpression Id. */
  int LEGALID = 55;

  /** Lexical state. */
  int DEFAULT = 0;

  /** Literal token values. */
  String[] tokenImage = {
    "<EOF>",
    "\"\\t\"",
    "\" \"",
    "\"\\n\"",
    "\"\\r\"",
    "\"\\r\\n\"",
    "\";\"",
    "\".\"",
    "\"*\"",
    "\"exit\"",
    "\"create\"",
    "\"drop\"",
    "\"table\"",
    "\"tables\"",
    "\"show\"",
    "\"desc\"",
    "\"select\"",
    "\"insert\"",
    "\"into\"",
    "\"from\"",
    "\"delete\"",
    "\"primary\"",
    "\"foreign\"",
    "\"key\"",
    "\"references\"",
    "\"is\"",
    "\"not\"",
    "\"null\"",
    "\"values\"",
    "\"as\"",
    "\"or\"",
    "\"and\"",
    "\"where\"",
    "\"int\"",
    "\"char\"",
    "\"date\"",
    "\"blowup\"",
    "<_ALPHABET>",
    "\"\\\'\"",
    "<_NONQUOTECHAR>",
    "<_SIGN>",
    "<_DIGIT>",
    "<_INTVALUE>",
    "<_CHARSTRING>",
    "<_DATEVALUE>",
    "\"_\"",
    "\"(\"",
    "\")\"",
    "<INTVALUE>",
    "<CHARSTRING>",
    "<DATEVALUE>",
    "<LEFTPAREN>",
    "<RIGHTPAREN>",
    "\",\"",
    "<COMPOP>",
    "<LEGALID>",
  };

}
