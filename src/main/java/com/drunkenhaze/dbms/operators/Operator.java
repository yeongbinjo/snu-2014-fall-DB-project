package com.drunkenhaze.dbms.operators;

import java.util.List;
import java.util.Map;

import com.drunkenhaze.dbms.components.Row;
import com.drunkenhaze.dbms.components.TableSchema;
import com.drunkenhaze.dbms.exceptions.SemanticError;

public interface Operator {
	public enum EvalResult { TRUE, FALSE, UNKNOWN };

	public EvalResult eval(List<Row> resultSet, List<TableSchema> targetTables,
			Map<String, Integer> tableIndexMap) throws SemanticError;
}
