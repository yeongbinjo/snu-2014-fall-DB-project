package com.drunkenhaze.dbms.main;

import java.io.StringReader;

import junit.framework.Test;
import junit.framework.TestCase;
import junit.framework.TestSuite;

import com.drunkenhaze.dbms.exceptions.SemanticError;
import com.drunkenhaze.dbms.managers.FileManager;

public class DMLTest extends TestCase {
	FileManager fileMgr;
	SQLParser parser;

	/**
	 * Create the test case
	 *
	 * @param testName name of the test case
	 */
	public DMLTest(String testName)
	{
		super( testName );
	}

	/**
	 * @return the suite of tests being tested
	 */
	public static Test suite()
	{
		return new TestSuite( DMLTest.class );
	}

	public void setUp() {
		if (FileManager.getDBPath() == null)
			FileManager.setDBPath(Constants.TEST_DB_PATH);
		fileMgr = FileManager.getInstance();
		fileMgr.blowup();
		parser = new SQLParser(System.in);
		parser.init();
		parser.setFileManager(fileMgr);
	}

	private void testSemanticSuccessQuery(String testQuery, boolean stackTrace) {
		StringReader sr = new StringReader(testQuery);
		parser.ReInit(sr);
		try {
			boolean keepGoing = true;
			while (keepGoing) {
				keepGoing = parser.parse();
				if (parser.isStreamEmpty()) keepGoing = false;
			}
		} catch (ParseException e) {
			if (stackTrace) e.printStackTrace();
			System.out.println("Unexpected ParseException");
			fail();
		} catch (SemanticError e) {
			if (stackTrace) e.printStackTrace();
			System.out.println("Unexpected SemanticError");
			fail();
		}
	}

	private void testSemanticErrorQuery(String testQuery, String expectMsg, boolean stackTrace) {
		StringReader sr = new StringReader(testQuery);
		parser.ReInit(sr);
		try {
			boolean keepGoing = true;
			while (keepGoing) {
				keepGoing = parser.parse();
				if (parser.isStreamEmpty()) keepGoing = false;
			}
			fail();
		} catch (ParseException e) {
			if (stackTrace) e.printStackTrace();
			System.out.println("Unexpected ParseException");
			fail();
		} catch (SemanticError e) {
			if (stackTrace) e.printStackTrace();
			parser.printShell(e.getMessage());
			assertEquals(expectMsg, e.getMessage());
		}
	}

	/*
	 * Test for success query
	 */
	public void testBaseCase() {
		// SET UP TABLE
		testSemanticSuccessQuery("create table customer(\n" +
				"	cust_id char(6),\n" +
				"	customer_name char(30),\n" +
				"	customer_street char(20),\n" +
				"	customer_city char (20),\n" +
				"	primary key (cust_id)\n" +
				");\n", false);
		testSemanticSuccessQuery("create table borrower(\n" +
				"	cust_id char(6),\n" +
				"	loan_number int,\n" +
				"	foreign key(cust_id) references customer(cust_id),\n" +
				"	primary key(loan_number)\n" +
				");", false);
		testSemanticSuccessQuery("create table account(\n" +
				"	account_number int not null,\n" +
				"	branch_name char(15),\n" +
				"	primary key(account_number)\n" +
				");", false);
		testSemanticSuccessQuery("create table branch(\n" +
				"	branch_name char(30) not null,\n" +
				"	assets int,\n" +
				"	branch_city char(20),\n" +
				"	primary key(branch_name)\n" +
				");\n", false);

		// INSERT 1
		testSemanticSuccessQuery("insert into account values(9732, 'Perryridge');", false);

		// INSERT 2
		testSemanticSuccessQuery("insert into branch (branch_name) values('DrunkenHaze');", false);

		// INSERT 3
		testSemanticSuccessQuery("insert into customer (cust_id) values('123456789');", false);

		// INSERT with Referential Integrity
		testSemanticSuccessQuery("insert into borrower values('123456', 123456);", false);

		// INSERT with Case Insensitivity Support
		testSemanticSuccessQuery("insert into branch (branch_NAME, Assets) values('TestBranch', 100000);", false);
		testSemanticSuccessQuery("select * from branch;", false);


		// SET UP TABLE AND INSERT DATAS
		testSemanticSuccessQuery("create table department (\n" +
				"		dept_id		char(30), \n" +
				"		dept_name 	char(30), \n" +
				"		office		char(30),  \n" +
				"		primary key(dept_id)\n" +
				");\n" +
				"create table student (\n" +
				"		stu_id 		char(30),\n" +
				"		resident_id 	char(42) not null,\n" +
				"		name 		char(30) not null,\n" +
				"		year 		int,\n" +
				"		address 		char(30),\n" +
				"		dept_id 		char(30),\n" +
				"		primary key(stu_id),\n" +
				"		foreign key(dept_id) references \n" +
				"					department(dept_id)\n" +
				");\n" +
				"create table professor 	(\n" +
				"		prof_id 	char(30) ,\n" +
				"		resident_id 	char(42) 	not null,\n" +
				"		name 		char(30) 	not null,\n" +
				"		dept_id 	char(30),\n" +
				"		position 	char(30),\n" +
				"		year_emp 	int,\n" +
				"		primary key(prof_id),\n" +
				"		foreign key(dept_id) \n" +
				"				references department(dept_id)\n" +
				");\n" +
				"create table course (\n" +
				"		course_id 	char(30) ,\n" +
				"		title 		char(42) 	not null,\n" +
				"		credit 		int,\n" +
				"		primary key(course_id)\n" +
				");\n" +
				"create table class (\n" +
				"		class_id 	char(30) ,\n" +
				"		course_id 	char(30),\n" +
				"		year 		int,\n" +
				"		semester	int,\n" +
				"		division	char(1),\n" +
				"		prof_id 	char(30),\n" +
				"		classroom 	char(9),\n" +
				"		enroll 		int,\n" +
				"		primary key(class_id),\n" +
				"		foreign key(course_id) \n" +
				"				references  	course(course_id),\n" +
				"		foreign key(prof_id) \n" +
				"				references 	professor(prof_id)\n" +
				");\n" +
				"create table takes (\n" +
				"		stu_id 		char(30) ,\n" +
				"		class_id 	char(30),\n" +
				"		grade 		char(5),\n" +
				"		primary key(stu_id, class_id),\n" +
				"		foreign key(stu_id) \n" +
				"				references 	student(stu_id),\n" +
				"		foreign key(class_id) \n" +
				"				references 	class(class_id)\n" +
				");\n", false);
		testSemanticSuccessQuery("insert into department values('920', 'Comp. Sci.', '201 room');", false);
		testSemanticSuccessQuery("insert into department values('923', 'Indust. Engn.', '207 room');", false);
		testSemanticSuccessQuery("insert into department values('925', 'Elec. Engn.', '308 room');", false);

		testSemanticSuccessQuery("insert into student values('1292001', '900424-1825409', '김광식', 3, 'Seoul', '920');", false);
		testSemanticSuccessQuery("insert into student values('1292002', '900305-1730021', '김정현', 3, 'Seoul', '920');", false);
		testSemanticSuccessQuery("insert into student values('1292003', '891021-2308302', '김현정', 4, 'Daejeon', '920');", false);
		testSemanticSuccessQuery("insert into student values('1292301', '890902-2704012', '김현정', 2, 'Taeku', '923');", false);
		testSemanticSuccessQuery("insert into student values('1292303', '910715-1524390', '박광수', 3, 'Gwangju', '923');", false);
		testSemanticSuccessQuery("insert into student values('1292305', '921011-1809003', '김우주', 4, 'Pusan', '923');", false);
		testSemanticSuccessQuery("insert into student values('1292501', '900825-1506390', '박철수', 3, 'Daejeon', '925');", false);
		testSemanticSuccessQuery("insert into student values('1292502', '911011-1809003', '백태성', 3, 'Seoul', '925');", false);

		testSemanticSuccessQuery("insert into professor values('92001', '590327-1******', '이태규', '920', 'Professor', 1997);", false);
		testSemanticSuccessQuery("insert into professor values('92002', '690702-1350026', '고희석', '920', 'Associate', 2003);", false);
		testSemanticSuccessQuery("insert into professor values('92301', '741011-2765501', '최성희', '923', 'Associate', 2005);", false);
		testSemanticSuccessQuery("insert into professor values('92302', '750728-1102458', '김태석', '923', 'Professor', 1999);", false);
		testSemanticSuccessQuery("insert into professor values('92501', '620505-1200546', '박철재', '925', 'Assistant', 2007);", false);
		testSemanticSuccessQuery("insert into professor values('92502', '740101-1830264', '장민석', '925', 'Associate', 2005);", false);

		testSemanticSuccessQuery("insert into course values('C101', '전산개론', 3);", false);
		testSemanticSuccessQuery("insert into course values('C102', '자료구조', 3);", false);
		testSemanticSuccessQuery("insert into course values('C103', '데이터베이스', 4);", false);
		testSemanticSuccessQuery("insert into course values('C301', '운영체제', 3);", false);
		testSemanticSuccessQuery("insert into course values('C302', '컴퓨터구조', 3);", false);
		testSemanticSuccessQuery("insert into course values('C303', '이산수학', 4);", false);
		testSemanticSuccessQuery("insert into course values('C304', '객체지향언어', 4);", false);
		testSemanticSuccessQuery("insert into course values('C501', '인공지능', 3);", false);
		testSemanticSuccessQuery("insert into course values('C502', '알고리즘', 2);", false);

		testSemanticSuccessQuery("insert into class values('C101-01', 'C101', 2012, 1, 'A', '92301', '301 room', 40);", false);
		testSemanticSuccessQuery("insert into class values('C102-01', 'C102', 2012, 1, 'A', '92001', '209 room', 30);", false);
		testSemanticSuccessQuery("insert into class values('C103-01', 'C103', 2012, 1, 'A', '92501', '208 room', 30);", false);
		testSemanticSuccessQuery("insert into class values('C103-02', 'C103', 2012, 1, 'B', '92301', '301 room', 30);", false);
		testSemanticSuccessQuery("insert into class values('C501-01', 'C501', 2012, 1, 'A', '92501', '103 room', 45);", false);
		testSemanticSuccessQuery("insert into class values('C501-02', 'C501', 2012, 1, 'B', '92502', '204 room', 25);", false);
		testSemanticSuccessQuery("insert into class values('C301-01', 'C301', 2012, 2, 'A', '92502', '301 room', 30);", false);
		testSemanticSuccessQuery("insert into class values('C302-01', 'C302', 2012, 2, 'A', '92501', '209 room', 45);", false);
		testSemanticSuccessQuery("insert into class values('C502-01', 'C502', 2012, 2, 'A', '92001', '209 room', 30);", false);
		testSemanticSuccessQuery("insert into class values('C502-02', 'C502', 2012, 2, 'B', '92301', '103 room', 26);", false);

		testSemanticSuccessQuery("insert into takes values('1292001', 'C101-01', 'B+');", false);
		testSemanticSuccessQuery("insert into takes values('1292001', 'C103-01', 'A+');", false);
		testSemanticSuccessQuery("insert into takes values('1292001', 'C301-01', 'A');", false);
		testSemanticSuccessQuery("insert into takes values('1292002', 'C102-01', 'A');", false);
		testSemanticSuccessQuery("insert into takes values('1292002', 'C103-01', 'B+');", false);
		testSemanticSuccessQuery("insert into takes values('1292002', 'C502-01', 'C+');", false);
		testSemanticSuccessQuery("insert into takes values('1292003', 'C103-02', 'B');", false);
		testSemanticSuccessQuery("insert into takes values('1292003', 'C501-02', 'A+');", false);
		testSemanticSuccessQuery("insert into takes values('1292301', 'C102-01', 'C+');", false);
		testSemanticSuccessQuery("insert into takes values('1292303', 'C102-01', 'C');", false);
		testSemanticSuccessQuery("insert into takes values('1292303', 'C103-02', 'B+');", false);
		testSemanticSuccessQuery("insert into takes values('1292303', 'C501-01', 'A+');", false);

		// SELECT 1
		testSemanticSuccessQuery("select * from department;", false);
		testSemanticSuccessQuery("select * from student;", false);
		testSemanticSuccessQuery("select * from professor;", false);
		testSemanticSuccessQuery("select * from course;", false);
		testSemanticSuccessQuery("select * from class;", false);
		testSemanticSuccessQuery("select * from takes;", false);

		// SELECT 2
		testSemanticSuccessQuery("select stu_id, name from student where year=2;", false);

		// SELECT 3
		testSemanticSuccessQuery("select class.course_id from student, takes, class, department\n" +
				"where\n" +
				"student.stu_id = takes.stu_id\n" +
				"and takes.class_id = class.class_id\n" +
				"and student.dept_id = department.dept_id\n" +
				"and department.dept_name = 'Indust. Engn.';", false);

		// SELECT 4
		testSemanticSuccessQuery("select name from student, takes, class\n" +
				"where\n" +
				"student.stu_id = takes.stu_id and takes.class_id = class.class_id\n" +
				"and class.year = 2012 and takes.grade = 'A+';", false);

		// SELECT 5
		testSemanticSuccessQuery("select stu_id from student, department\n" +
				"where\n" +
				"student.dept_id = department.dept_id\n" +
				"and department.dept_name = 'Comp. Sci.';", false);

		// SELECT 6
		testSemanticSuccessQuery("select name as StudentName from student as st where student.stu_id = '1292501';", false);

		// SELECT All Columns with Join Operation
		testSemanticSuccessQuery("select * from student, professor;", false);

		// SELECT with Case Insensitivity Support
		testSemanticSuccessQuery("select * from STUDENT where sTuDeNt.NAme = '김현정';", false);

		// SELECT with Natural Join Condition
		testSemanticSuccessQuery("select * from student, TaKeS where sTuDeNt.sTu_Id = TaKeS.StU_iD;", false);

		// DELETE 1
		testSemanticSuccessQuery("delete from student where student.stu_id = '1292501';", false);
		testSemanticSuccessQuery("select name as StudentName from student as st where student.stu_id = '1292501';", false);

		// DELETE 2
		testSemanticSuccessQuery("select * from takes;", false);
		testSemanticSuccessQuery("delete from takes;", false);
		testSemanticSuccessQuery("select * from takes;", false);

		// DELETE with CASCADE UPDATE 1
		testSemanticSuccessQuery("delete from student where student.stu_id = '1292501';", false);
		testSemanticSuccessQuery("select name as StudentName from student as st where student.stu_id = '1292501';", false);

		// DELETE with CASCADE UPDATE 2
		testSemanticSuccessQuery("select * from class;", false);
		testSemanticSuccessQuery("delete from course where course.title = '자료구조';", false);
		testSemanticSuccessQuery("select * from class;", false);

		// DELETE with CASCADE UPDATE 3
		testSemanticSuccessQuery("select * from class;", false);
		testSemanticSuccessQuery("delete from professor where dept_id = '923';", false);
		testSemanticSuccessQuery("select * from class;", false);
	}

	public void testCaseInsensitivity() {
		// SET UP TABLE AND INSERT DATAS
		testSemanticSuccessQuery("create table dEpArTmEnT (\n" +
				"		dePT_ID		char(30), \n" +
				"		dept_NAME 	char(30), \n" +
				"		OFFICe		char(30),  \n" +
				"		primary key(dePt_id)\n" +
				");\n" +
				"create table stUdEnt (\n" +
				"		stu_Id 		char(30),\n" +
				"		reSident_id 	char(42) not null,\n" +
				"		naME 		char(30) not null,\n" +
				"		YeaR 		int,\n" +
				"		aDDress 		char(30),\n" +
				"		depT_Id 		char(30),\n" +
				"		primary key(Stu_id),\n" +
				"		foreign key(dept_Id) references \n" +
				"					depaRtment(dEpt_id)\n" +
				");\n" +
				"create table profeSSor 	(\n" +
				"		pROf_id 	char(30) ,\n" +
				"		reSIdent_id 	char(42) 	not null,\n" +
				"		NamE 		char(30) 	not null,\n" +
				"		dePT_Id 	char(30),\n" +
				"		poSITion 	char(30),\n" +
				"		yEAR_emp 	int,\n" +
				"		primary key(prOF_id),\n" +
				"		foreign key(dEPt_id) \n" +
				"				references deparTmeNT(DepT_ID)\n" +
				");\n" +
				"create table course (\n" +
				"		course_id 	char(30) ,\n" +
				"		title 		char(42) 	not null,\n" +
				"		credit 		int,\n" +
				"		primary key(course_id)\n" +
				");\n" +
				"create table class (\n" +
				"		class_id 	char(30) ,\n" +
				"		course_id 	char(30),\n" +
				"		year 		int,\n" +
				"		semester	int,\n" +
				"		division	char(1),\n" +
				"		prof_id 	char(30),\n" +
				"		classroom 	char(9),\n" +
				"		enroll 		int,\n" +
				"		primary key(class_id),\n" +
				"		foreign key(course_id) \n" +
				"				references  	course(course_id),\n" +
				"		foreign key(prof_id) \n" +
				"				references 	professor(prof_id)\n" +
				");\n" +
				"create table takes (\n" +
				"		stu_id 		char(30) ,\n" +
				"		class_id 	char(30),\n" +
				"		grade 		char(5),\n" +
				"		primary key(stu_id, class_id),\n" +
				"		foreign key(stu_id) \n" +
				"				references 	student(stu_id),\n" +
				"		foreign key(class_id) \n" +
				"				references 	class(class_id)\n" +
				");\n", false);
		testSemanticSuccessQuery("insert into depaRTment values('920', 'Comp. Sci.', '201 room');", false);
		testSemanticSuccessQuery("insert into deparTMent values('923', 'Indust. Engn.', '207 room');", false);
		testSemanticSuccessQuery("insert into departMEnt values('925', 'Elec. Engn.', '308 room');", false);

		testSemanticSuccessQuery("insert into StudenT values('1292001', '900424-1825409', '김광식', 3, 'Seoul', '920');", false);
		testSemanticSuccessQuery("insert into STudent values('1292002', '900305-1730021', '김정현', 3, 'Seoul', '920');", false);
		testSemanticSuccessQuery("insert into sTUdent values('1292003', '891021-2308302', '김현정', 4, 'Daejeon', '920');", false);
		testSemanticSuccessQuery("insert into stUDent values('1292301', '890902-2704012', '김현정', 2, 'Taeku', '923');", false);
		testSemanticSuccessQuery("insert into stuDEnt values('1292303', '910715-1524390', '박광수', 3, 'Gwangju', '923');", false);
		testSemanticSuccessQuery("insert into studENt values('1292305', '921011-1809003', '김우주', 4, 'Pusan', '923');", false);
		testSemanticSuccessQuery("insert into studeNT values('1292501', '900825-1506390', '박철수', 3, 'Daejeon', '925');", false);
		testSemanticSuccessQuery("insert into StudenT values('1292502', '911011-1809003', '백태성', 3, 'Seoul', '925');", false);

		testSemanticSuccessQuery("insert into PROfessor values('92001', '590327-1******', '이태규', '920', 'Professor', 1997);", false);
		testSemanticSuccessQuery("insert into pROFessor values('92002', '690702-1350026', '고희석', '920', 'Associate', 2003);", false);
		testSemanticSuccessQuery("insert into prOFEssor values('92301', '741011-2765501', '최성희', '923', 'Associate', 2005);", false);
		testSemanticSuccessQuery("insert into proFESsor values('92302', '750728-1102458', '김태석', '923', 'Professor', 1999);", false);
		testSemanticSuccessQuery("insert into profESSor values('92501', '620505-1200546', '박철재', '925', 'Assistant', 2007);", false);
		testSemanticSuccessQuery("insert into profeSSOr values('92502', '740101-1830264', '장민석', '925', 'Associate', 2005);", false);

		testSemanticSuccessQuery("insert into cOuRse values('C101', '전산개론', 3);", false);
		testSemanticSuccessQuery("insert into coUrSe values('C102', '자료구조', 3);", false);
		testSemanticSuccessQuery("insert into couRsE values('C103', '데이터베이스', 4);", false);
		testSemanticSuccessQuery("insert into CourSe values('C301', '운영체제', 3);", false);
		testSemanticSuccessQuery("insert into cOursE values('C302', '컴퓨터구조', 3);", false);
		testSemanticSuccessQuery("insert into CoUrse values('C303', '이산수학', 4);", false);
		testSemanticSuccessQuery("insert into cOuRse values('C304', '객체지향언어', 4);", false);
		testSemanticSuccessQuery("insert into coUrSe values('C501', '인공지능', 3);", false);
		testSemanticSuccessQuery("insert into couRsE values('C502', '알고리즘', 2);", false);

		testSemanticSuccessQuery("insert into clASS values('C101-01', 'C101', 2012, 1, 'A', '92301', '301 room', 40);", false);
		testSemanticSuccessQuery("insert into CLAss values('C102-01', 'C102', 2012, 1, 'A', '92001', '209 room', 30);", false);
		testSemanticSuccessQuery("insert into cLASs values('C103-01', 'C103', 2012, 1, 'A', '92501', '208 room', 30);", false);
		testSemanticSuccessQuery("insert into ClasS values('C103-02', 'C103', 2012, 1, 'B', '92301', '301 room', 30);", false);
		testSemanticSuccessQuery("insert into CLASs values('C501-01', 'C501', 2012, 1, 'A', '92501', '103 room', 45);", false);
		testSemanticSuccessQuery("insert into class values('C501-02', 'C501', 2012, 1, 'B', '92502', '204 room', 25);", false);
		testSemanticSuccessQuery("insert into clAss values('C301-01', 'C301', 2012, 2, 'A', '92502', '301 room', 30);", false);
		testSemanticSuccessQuery("insert into claSs values('C302-01', 'C302', 2012, 2, 'A', '92501', '209 room', 45);", false);
		testSemanticSuccessQuery("insert into cLass values('C502-01', 'C502', 2012, 2, 'A', '92001', '209 room', 30);", false);
		testSemanticSuccessQuery("insert into cLaSs values('C502-02', 'C502', 2012, 2, 'B', '92301', '103 room', 26);", false);

		testSemanticSuccessQuery("insert into takes values('1292001', 'C101-01', 'B+');", false);
		testSemanticSuccessQuery("insert into takes values('1292001', 'C103-01', 'A+');", false);
		testSemanticSuccessQuery("insert into takes values('1292001', 'C301-01', 'A');", false);
		testSemanticSuccessQuery("insert into takes values('1292002', 'C102-01', 'A');", false);
		testSemanticSuccessQuery("insert into takes values('1292002', 'C103-01', 'B+');", false);
		testSemanticSuccessQuery("insert into takes values('1292002', 'C502-01', 'C+');", false);
		testSemanticSuccessQuery("insert into takes values('1292003', 'C103-02', 'B');", false);
		testSemanticSuccessQuery("insert into takes values('1292003', 'C501-02', 'A+');", false);
		testSemanticSuccessQuery("insert into takes values('1292301', 'C102-01', 'C+');", false);
		testSemanticSuccessQuery("insert into takes values('1292303', 'C102-01', 'C');", false);
		testSemanticSuccessQuery("insert into takes values('1292303', 'C103-02', 'B+');", false);
		testSemanticSuccessQuery("insert into takes values('1292303', 'C501-01', 'A+');", false);

		testSemanticSuccessQuery("select nAMe, dept_name from sTuDeNT, dEPARTMENt\n" +
				"where studENt.dePT_ID = dePARTment.DEPt_id;", false);
	}

	public void testCascadeDeletion() {
		// CREATE TABLE and INSERT for CASCADE DELETION TEST 1
		testSemanticSuccessQuery("create table Students(id int, primary key(id));" , false);
		testSemanticSuccessQuery("create table Enrolls(id int, lecture_name char(50), foreign key (id) references students (id));", false);

		testSemanticSuccessQuery("insert into Students values(1001);", false);
		testSemanticSuccessQuery("insert into Students values(1002);", false);

		testSemanticSuccessQuery("insert into Enrolls values(1001, 'Database');", true);
		testSemanticSuccessQuery("insert into Enrolls values(1001, 'Algorithm');", false);

		testSemanticErrorQuery("insert into Enrolls values(1003, 'Database');",
				Messages.InsertReferentialIntegrityError, false);

		testSemanticSuccessQuery("select * from Students;", false);
		testSemanticSuccessQuery("select * from Enrolls;", false);

		testSemanticSuccessQuery("delete from Students where ID = 1001;", false);
		testSemanticSuccessQuery("select * from Students;", false);
		testSemanticSuccessQuery("select * from Enrolls;", false);

		fileMgr.blowup();

		// CREATE TABLE and INSERT for CASCADE DELETION TEST 2
		testSemanticSuccessQuery("create table Students(id int, primary key(id));" , false);
		testSemanticSuccessQuery("create table Enrolls(id int not null, lecture_name char(50), foreign key (id) references students (id));", false);

		testSemanticSuccessQuery("insert into Students values(1001);", false);
		testSemanticSuccessQuery("insert into Students values(1002);", false);

		testSemanticSuccessQuery("insert into Enrolls values(1001, 'Database');", true);
		testSemanticSuccessQuery("insert into Enrolls values(1001, 'Algorithm');", false);

		testSemanticSuccessQuery("select * from Students;", false);
		testSemanticSuccessQuery("select * from Enrolls;", false);

		testSemanticSuccessQuery("delete from Students where ID = 1001;", false);
		testSemanticSuccessQuery("select * from Students;", false);
		testSemanticSuccessQuery("select * from Enrolls;", false);

		fileMgr.blowup();

		// CREATE TABLE and INSERT for CASCADE DELETION TEST 3
		testSemanticSuccessQuery("create table Students(id int, primary key(id));" , false);
		testSemanticSuccessQuery("create table Students_other(id int, primary key(id));" , false);
		testSemanticSuccessQuery("create table Enrolls(id int, lecture_name char(50),\n" +
				"foreign key (id) references students (id),\n" +
				"foreign key (id) references students_other(id));", false);

		testSemanticSuccessQuery("insert into Students values(1001);", false);
		testSemanticSuccessQuery("insert into Students values(1002);", false);

		testSemanticSuccessQuery("insert into Students_other values(1002);", false);
		testSemanticSuccessQuery("insert into Students_other values(1003);", false);

		testSemanticSuccessQuery("insert into Enrolls values(1001, 'Database');", true);
		testSemanticSuccessQuery("insert into Enrolls values(1001, 'Algorithm');", false);
		testSemanticSuccessQuery("insert into Enrolls values(1002, 'OS');", false);
		testSemanticSuccessQuery("insert into Enrolls values(1003, 'OS');", false);

		testSemanticSuccessQuery("select * from Students;", false);
		testSemanticSuccessQuery("select * from Students_other;", false);
		testSemanticSuccessQuery("select * from Enrolls;", false);

		testSemanticSuccessQuery("delete from Students where ID = 1001;", false);
		testSemanticSuccessQuery("select * from Students;", false);
		testSemanticSuccessQuery("select * from Students_other;", false);
		testSemanticSuccessQuery("select * from Enrolls;", false);

		testSemanticSuccessQuery("delete from Students_other where ID = 1003;", false);
		testSemanticSuccessQuery("select * from Students;", false);
		testSemanticSuccessQuery("select * from Students_other;", false);
		testSemanticSuccessQuery("select * from Enrolls;", false);

		testSemanticSuccessQuery("insert into Enrolls values(null, 'Network');", false);
		testSemanticSuccessQuery("select * from Enrolls;", false);

		fileMgr.blowup();

		// CREATE TABLE and INSERT for CASCADE DELETION TEST 4
		testSemanticSuccessQuery("create table Students(id int, primary key(id));" , false);
		testSemanticSuccessQuery("create table Enrolls(id int not null, lecture_name char(50), foreign key (id) references students (id));", false);

		testSemanticSuccessQuery("insert into Students values(1001);", false);
		testSemanticSuccessQuery("insert into Students values(1002);", false);

		testSemanticSuccessQuery("insert into Enrolls values(1001, 'Database');", true);
		testSemanticSuccessQuery("insert into Enrolls values(1001, 'Algorithm');", false);

		testSemanticSuccessQuery("select * from Students;", false);
		testSemanticSuccessQuery("select * from Enrolls;", false);

		testSemanticSuccessQuery("delete from Students where ID = 1001;", false);
		testSemanticSuccessQuery("select * from Students;", false);
		testSemanticSuccessQuery("select * from Enrolls;", false);

		testSemanticSuccessQuery("drop table enrolls;", false);
		testSemanticSuccessQuery("delete from students where ID = 1001;", false);
		testSemanticSuccessQuery("select * from Students;", false);
	}

	/*
	 * Test for semantic error query
	 */
	public void testSemanticErrorCase() {
		// SET UP TABLE
		testSemanticSuccessQuery("create table customer(\n" +
				"	cust_id char(6),\n" +
				"	customer_name char(30),\n" +
				"	customer_street char(20),\n" +
				"	customer_city char (20),\n" +
				"	primary key (cust_id)\n" +
				");\n", false);
		testSemanticSuccessQuery("create table borrower(\n" +
				"	cust_id char(6),\n" +
				"	loan_number int,\n" +
				"	foreign key(cust_id) references customer(cust_id),\n" +
				"	primary key(loan_number)\n" +
				");", false);
		testSemanticSuccessQuery("create table account(\n" +
				"	account_number int not null,\n" +
				"	branch_name char(15),\n" +
				"	primary key(account_number)\n" +
				");", false);
		testSemanticSuccessQuery("create table branch(\n" +
				"	branch_name char(30) not null,\n" +
				"	assets int,\n" +
				"	branch_city char(20),\n" +
				"	primary key(branch_name)\n" +
				");\n", false);

		// InsertDuplicatePrimaryKeyError
		testSemanticSuccessQuery("insert into account values(9732, 'Perryridge');", false);
		testSemanticErrorQuery("insert into account values(9732, 'DrunkenHaze');",
				Messages.InsertDuplicatePrimaryKeyError, false);

		// InsertTypeMismatchError 1
		testSemanticErrorQuery("insert into account values('9733', 'DrunkenHaze');",
				Messages.InsertTypeMismatchError, false);

		// InsertTypeMismatchError 2
		testSemanticErrorQuery("insert into account values(9733, 'DrunkenHaze', 2014-10-31);",
				Messages.InsertTypeMismatchError, false);

		// InsertTypeMismatchError 3
		testSemanticErrorQuery("insert into account values(9733);",
				Messages.InsertTypeMismatchError, false);

		// InsertColumnExistenceError
		testSemanticErrorQuery("insert into customer (customer_id, customer_name) values('123456', 'Yeongbin');",
				String.format(Messages.InsertColumnExistenceError, "customer_id"), false);

		// InsertColumnNonNullableError
		testSemanticErrorQuery("insert into branch (branch_city, assets) values('seoul', 10000);",
				String.format(Messages.InsertColumnNonNullableError, "branch_name"), false);

		// InsertColumnDuplicatedError
		testSemanticErrorQuery("insert into branch (branch_name, branch_city, branch_city, assets)\n" +
				"values('DrunkenHaze', 'seoul', 'kanglung', 10000);",
				String.format(Messages.InsertColumnDuplicatedError, "branch_city"), false);

		// InsertReferentialIntegrityError
		testSemanticErrorQuery("insert into borrower values('123456', 123456);",
				Messages.InsertReferentialIntegrityError, false);

		// SelectTableExistenceError
		testSemanticErrorQuery("select * from barrower;",
				String.format(Messages.SelectTableExistenceError, "barrower"), false);

		// SelectColumnResolveError
		testSemanticErrorQuery("select loan_num from borrower;",
				String.format(Messages.SelectColumnResolveError, "loan_num"), false);

		// WhereIncomparableError
		testSemanticErrorQuery("select * from account where account.account_number < '10000';",
				Messages.WhereIncomparableError, false);

		// WhereTableNotSpecified
		testSemanticErrorQuery("select * from account where account.account_number < 10000 and branch.city = 'seoul';",
				Messages.WhereTableNotSpecified, false);

		// WhereColumnNotExist
		testSemanticErrorQuery("select * from account where account.account_number < 10000 and city = 'seoul';",
				Messages.WhereColumnNotExist, false);

		// WhereAmbiguousReference 1
		testSemanticSuccessQuery("insert into branch (branch_name, branch_city, assets)\n" +
				"values('DrunkenHaze', 'seoul', 10000);", false);
		testSemanticErrorQuery("select * from account, branch where branch_name = 'DrunkenHaze';",
				Messages.WhereAmbiguousReference, false);

		// SelectColumnResolveError
		testSemanticErrorQuery("select branch_name from account, branch;",
				String.format(Messages.SelectColumnResolveError, "branch_name"), false);

		// InvalidDateRangeError
		testSemanticSuccessQuery("create table a(b date);", false);
		testSemanticErrorQuery("insert into a values(2014-01-32);", Messages.InvalidDateRangeError, false);
	}

}
