package com.drunkenhaze.dbms.components;

import java.io.Serializable;

public class DateDataType implements ColumnDataType, Serializable {
	private static final long serialVersionUID = -2710434190053489821L;

	@Override
	public String toString() {
		return "date";
	}

	@Override
	public boolean equals(Object other) {
		if (other == null) return false;
		if (other == this) return true;
		if (!(other instanceof DateDataType)) return false;
		return true;
	}
}
