package com.drunkenhaze.dbms.components;

import java.io.Serializable;

import com.drunkenhaze.dbms.exceptions.SemanticError;
import com.drunkenhaze.dbms.main.Messages;

public class CharDataType implements ColumnDataType, Serializable {
	private static final long serialVersionUID = -910844184638675128L;
	private int limit;

	public CharDataType(int limit) throws SemanticError {
		if (limit < 1) throw new SemanticError(Messages.CharLengthError);
		this.limit = limit;
	}

	public int getLimit() {
		return limit;
	}

	@Override
	public String toString() {
		return "char(" + limit + ")";
	}

	@Override
	public boolean equals(Object other) {
		if (other == null) return false;
		if (other == this) return true;
		if (!(other instanceof CharDataType)) return false;
		if (limit != ((CharDataType) other).limit) return false;
		return true;
	}
}
