/* Generated By:JavaCC: Do not edit this line. SQLParserTokenManager.java */
package com.drunkenhaze.dbms.main;
import java.util.List;
import java.util.LinkedList;
import java.util.Date;
import java.lang.String;
import java.lang.Integer;
import java.io.FileInputStream;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import org.apache.commons.lang3.tuple.Pair;
import com.drunkenhaze.dbms.components.*;
import com.drunkenhaze.dbms.exceptions.*;
import com.drunkenhaze.dbms.managers.*;
import com.drunkenhaze.dbms.operators.*;

/** Token Manager. */
public class SQLParserTokenManager implements SQLParserConstants
{

  /** Debug output. */
  public  java.io.PrintStream debugStream = System.out;
  /** Set debug output. */
  public  void setDebugStream(java.io.PrintStream ds) { debugStream = ds; }
private final int jjStopStringLiteralDfa_0(int pos, long active0)
{
   switch (pos)
   {
      case 0:
         if ((active0 & 0x1ffffffe00L) != 0L)
         {
            jjmatchedKind = 55;
            return 13;
         }
         return -1;
      case 1:
         if ((active0 & 0x1f9dfffe00L) != 0L)
         {
            jjmatchedKind = 55;
            jjmatchedPos = 1;
            return 13;
         }
         if ((active0 & 0x62000000L) != 0L)
            return 13;
         return -1;
      case 2:
         if ((active0 & 0x284840000L) != 0L)
            return 13;
         if ((active0 & 0x1d197bfe00L) != 0L)
         {
            if (jjmatchedPos != 2)
            {
               jjmatchedKind = 55;
               jjmatchedPos = 2;
            }
            return 13;
         }
         return -1;
      case 3:
         if ((active0 & 0x1111733400L) != 0L)
         {
            jjmatchedKind = 55;
            jjmatchedPos = 3;
            return 13;
         }
         if ((active0 & 0xc080cca00L) != 0L)
            return 13;
         return -1;
      case 4:
         if ((active0 & 0x1011730400L) != 0L)
         {
            if (jjmatchedPos != 4)
            {
               jjmatchedKind = 55;
               jjmatchedPos = 4;
            }
            return 13;
         }
         if ((active0 & 0x100003000L) != 0L)
            return 13;
         return -1;
      case 5:
         if ((active0 & 0x1010132400L) != 0L)
            return 13;
         if ((active0 & 0x1600000L) != 0L)
         {
            jjmatchedKind = 55;
            jjmatchedPos = 5;
            return 13;
         }
         return -1;
      case 6:
         if ((active0 & 0x1000000L) != 0L)
         {
            jjmatchedKind = 55;
            jjmatchedPos = 6;
            return 13;
         }
         if ((active0 & 0x600000L) != 0L)
            return 13;
         return -1;
      case 7:
         if ((active0 & 0x1000000L) != 0L)
         {
            jjmatchedKind = 55;
            jjmatchedPos = 7;
            return 13;
         }
         return -1;
      case 8:
         if ((active0 & 0x1000000L) != 0L)
         {
            jjmatchedKind = 55;
            jjmatchedPos = 8;
            return 13;
         }
         return -1;
      default :
         return -1;
   }
}
private final int jjStartNfa_0(int pos, long active0)
{
   return jjMoveNfa_0(jjStopStringLiteralDfa_0(pos, active0), pos + 1);
}
private int jjStopAtPos(int pos, int kind)
{
   jjmatchedKind = kind;
   jjmatchedPos = pos;
   return pos + 1;
}
private int jjMoveStringLiteralDfa0_0()
{
   switch(curChar)
   {
      case 9:
         return jjStopAtPos(0, 1);
      case 10:
         return jjStopAtPos(0, 3);
      case 13:
         jjmatchedKind = 4;
         return jjMoveStringLiteralDfa1_0(0x20L);
      case 32:
         return jjStopAtPos(0, 2);
      case 42:
         return jjStopAtPos(0, 8);
      case 44:
         return jjStopAtPos(0, 53);
      case 46:
         return jjStopAtPos(0, 7);
      case 59:
         return jjStopAtPos(0, 6);
      case 97:
         return jjMoveStringLiteralDfa1_0(0xa0000000L);
      case 98:
         return jjMoveStringLiteralDfa1_0(0x1000000000L);
      case 99:
         return jjMoveStringLiteralDfa1_0(0x400000400L);
      case 100:
         return jjMoveStringLiteralDfa1_0(0x800108800L);
      case 101:
         return jjMoveStringLiteralDfa1_0(0x200L);
      case 102:
         return jjMoveStringLiteralDfa1_0(0x480000L);
      case 105:
         return jjMoveStringLiteralDfa1_0(0x202060000L);
      case 107:
         return jjMoveStringLiteralDfa1_0(0x800000L);
      case 110:
         return jjMoveStringLiteralDfa1_0(0xc000000L);
      case 111:
         return jjMoveStringLiteralDfa1_0(0x40000000L);
      case 112:
         return jjMoveStringLiteralDfa1_0(0x200000L);
      case 114:
         return jjMoveStringLiteralDfa1_0(0x1000000L);
      case 115:
         return jjMoveStringLiteralDfa1_0(0x14000L);
      case 116:
         return jjMoveStringLiteralDfa1_0(0x3000L);
      case 118:
         return jjMoveStringLiteralDfa1_0(0x10000000L);
      case 119:
         return jjMoveStringLiteralDfa1_0(0x100000000L);
      default :
         return jjMoveNfa_0(0, 0);
   }
}
private int jjMoveStringLiteralDfa1_0(long active0)
{
   try { curChar = input_stream.readChar(); }
   catch(java.io.IOException e) {
      jjStopStringLiteralDfa_0(0, active0);
      return 1;
   }
   switch(curChar)
   {
      case 10:
         if ((active0 & 0x20L) != 0L)
            return jjStopAtPos(1, 5);
         break;
      case 97:
         return jjMoveStringLiteralDfa2_0(active0, 0x810003000L);
      case 101:
         return jjMoveStringLiteralDfa2_0(active0, 0x1918000L);
      case 104:
         return jjMoveStringLiteralDfa2_0(active0, 0x500004000L);
      case 108:
         return jjMoveStringLiteralDfa2_0(active0, 0x1000000000L);
      case 110:
         return jjMoveStringLiteralDfa2_0(active0, 0x280060000L);
      case 111:
         return jjMoveStringLiteralDfa2_0(active0, 0x4400000L);
      case 114:
         if ((active0 & 0x40000000L) != 0L)
            return jjStartNfaWithStates_0(1, 30, 13);
         return jjMoveStringLiteralDfa2_0(active0, 0x280c00L);
      case 115:
         if ((active0 & 0x2000000L) != 0L)
            return jjStartNfaWithStates_0(1, 25, 13);
         else if ((active0 & 0x20000000L) != 0L)
            return jjStartNfaWithStates_0(1, 29, 13);
         break;
      case 117:
         return jjMoveStringLiteralDfa2_0(active0, 0x8000000L);
      case 120:
         return jjMoveStringLiteralDfa2_0(active0, 0x200L);
      default :
         break;
   }
   return jjStartNfa_0(0, active0);
}
private int jjMoveStringLiteralDfa2_0(long old0, long active0)
{
   if (((active0 &= old0)) == 0L)
      return jjStartNfa_0(0, old0);
   try { curChar = input_stream.readChar(); }
   catch(java.io.IOException e) {
      jjStopStringLiteralDfa_0(1, active0);
      return 2;
   }
   switch(curChar)
   {
      case 97:
         return jjMoveStringLiteralDfa3_0(active0, 0x400000000L);
      case 98:
         return jjMoveStringLiteralDfa3_0(active0, 0x3000L);
      case 100:
         if ((active0 & 0x80000000L) != 0L)
            return jjStartNfaWithStates_0(2, 31, 13);
         break;
      case 101:
         return jjMoveStringLiteralDfa3_0(active0, 0x100000400L);
      case 102:
         return jjMoveStringLiteralDfa3_0(active0, 0x1000000L);
      case 105:
         return jjMoveStringLiteralDfa3_0(active0, 0x200200L);
      case 108:
         return jjMoveStringLiteralDfa3_0(active0, 0x18110000L);
      case 111:
         return jjMoveStringLiteralDfa3_0(active0, 0x1000084800L);
      case 114:
         return jjMoveStringLiteralDfa3_0(active0, 0x400000L);
      case 115:
         return jjMoveStringLiteralDfa3_0(active0, 0x28000L);
      case 116:
         if ((active0 & 0x4000000L) != 0L)
            return jjStartNfaWithStates_0(2, 26, 13);
         else if ((active0 & 0x200000000L) != 0L)
         {
            jjmatchedKind = 33;
            jjmatchedPos = 2;
         }
         return jjMoveStringLiteralDfa3_0(active0, 0x800040000L);
      case 121:
         if ((active0 & 0x800000L) != 0L)
            return jjStartNfaWithStates_0(2, 23, 13);
         break;
      default :
         break;
   }
   return jjStartNfa_0(1, active0);
}
private int jjMoveStringLiteralDfa3_0(long old0, long active0)
{
   if (((active0 &= old0)) == 0L)
      return jjStartNfa_0(1, old0);
   try { curChar = input_stream.readChar(); }
   catch(java.io.IOException e) {
      jjStopStringLiteralDfa_0(2, active0);
      return 3;
   }
   switch(curChar)
   {
      case 97:
         return jjMoveStringLiteralDfa4_0(active0, 0x400L);
      case 99:
         if ((active0 & 0x8000L) != 0L)
            return jjStartNfaWithStates_0(3, 15, 13);
         break;
      case 101:
         if ((active0 & 0x800000000L) != 0L)
            return jjStartNfaWithStates_0(3, 35, 13);
         return jjMoveStringLiteralDfa4_0(active0, 0x1530000L);
      case 108:
         if ((active0 & 0x8000000L) != 0L)
            return jjStartNfaWithStates_0(3, 27, 13);
         return jjMoveStringLiteralDfa4_0(active0, 0x3000L);
      case 109:
         if ((active0 & 0x80000L) != 0L)
            return jjStartNfaWithStates_0(3, 19, 13);
         return jjMoveStringLiteralDfa4_0(active0, 0x200000L);
      case 111:
         if ((active0 & 0x40000L) != 0L)
            return jjStartNfaWithStates_0(3, 18, 13);
         break;
      case 112:
         if ((active0 & 0x800L) != 0L)
            return jjStartNfaWithStates_0(3, 11, 13);
         break;
      case 114:
         if ((active0 & 0x400000000L) != 0L)
            return jjStartNfaWithStates_0(3, 34, 13);
         return jjMoveStringLiteralDfa4_0(active0, 0x100000000L);
      case 116:
         if ((active0 & 0x200L) != 0L)
            return jjStartNfaWithStates_0(3, 9, 13);
         break;
      case 117:
         return jjMoveStringLiteralDfa4_0(active0, 0x10000000L);
      case 119:
         if ((active0 & 0x4000L) != 0L)
            return jjStartNfaWithStates_0(3, 14, 13);
         return jjMoveStringLiteralDfa4_0(active0, 0x1000000000L);
      default :
         break;
   }
   return jjStartNfa_0(2, active0);
}
private int jjMoveStringLiteralDfa4_0(long old0, long active0)
{
   if (((active0 &= old0)) == 0L)
      return jjStartNfa_0(2, old0);
   try { curChar = input_stream.readChar(); }
   catch(java.io.IOException e) {
      jjStopStringLiteralDfa_0(3, active0);
      return 4;
   }
   switch(curChar)
   {
      case 97:
         return jjMoveStringLiteralDfa5_0(active0, 0x200000L);
      case 99:
         return jjMoveStringLiteralDfa5_0(active0, 0x10000L);
      case 101:
         if ((active0 & 0x1000L) != 0L)
         {
            jjmatchedKind = 12;
            jjmatchedPos = 4;
         }
         else if ((active0 & 0x100000000L) != 0L)
            return jjStartNfaWithStates_0(4, 32, 13);
         return jjMoveStringLiteralDfa5_0(active0, 0x10002000L);
      case 105:
         return jjMoveStringLiteralDfa5_0(active0, 0x400000L);
      case 114:
         return jjMoveStringLiteralDfa5_0(active0, 0x1020000L);
      case 116:
         return jjMoveStringLiteralDfa5_0(active0, 0x100400L);
      case 117:
         return jjMoveStringLiteralDfa5_0(active0, 0x1000000000L);
      default :
         break;
   }
   return jjStartNfa_0(3, active0);
}
private int jjMoveStringLiteralDfa5_0(long old0, long active0)
{
   if (((active0 &= old0)) == 0L)
      return jjStartNfa_0(3, old0);
   try { curChar = input_stream.readChar(); }
   catch(java.io.IOException e) {
      jjStopStringLiteralDfa_0(4, active0);
      return 5;
   }
   switch(curChar)
   {
      case 101:
         if ((active0 & 0x400L) != 0L)
            return jjStartNfaWithStates_0(5, 10, 13);
         else if ((active0 & 0x100000L) != 0L)
            return jjStartNfaWithStates_0(5, 20, 13);
         return jjMoveStringLiteralDfa6_0(active0, 0x1000000L);
      case 103:
         return jjMoveStringLiteralDfa6_0(active0, 0x400000L);
      case 112:
         if ((active0 & 0x1000000000L) != 0L)
            return jjStartNfaWithStates_0(5, 36, 13);
         break;
      case 114:
         return jjMoveStringLiteralDfa6_0(active0, 0x200000L);
      case 115:
         if ((active0 & 0x2000L) != 0L)
            return jjStartNfaWithStates_0(5, 13, 13);
         else if ((active0 & 0x10000000L) != 0L)
            return jjStartNfaWithStates_0(5, 28, 13);
         break;
      case 116:
         if ((active0 & 0x10000L) != 0L)
            return jjStartNfaWithStates_0(5, 16, 13);
         else if ((active0 & 0x20000L) != 0L)
            return jjStartNfaWithStates_0(5, 17, 13);
         break;
      default :
         break;
   }
   return jjStartNfa_0(4, active0);
}
private int jjMoveStringLiteralDfa6_0(long old0, long active0)
{
   if (((active0 &= old0)) == 0L)
      return jjStartNfa_0(4, old0);
   try { curChar = input_stream.readChar(); }
   catch(java.io.IOException e) {
      jjStopStringLiteralDfa_0(5, active0);
      return 6;
   }
   switch(curChar)
   {
      case 110:
         if ((active0 & 0x400000L) != 0L)
            return jjStartNfaWithStates_0(6, 22, 13);
         return jjMoveStringLiteralDfa7_0(active0, 0x1000000L);
      case 121:
         if ((active0 & 0x200000L) != 0L)
            return jjStartNfaWithStates_0(6, 21, 13);
         break;
      default :
         break;
   }
   return jjStartNfa_0(5, active0);
}
private int jjMoveStringLiteralDfa7_0(long old0, long active0)
{
   if (((active0 &= old0)) == 0L)
      return jjStartNfa_0(5, old0);
   try { curChar = input_stream.readChar(); }
   catch(java.io.IOException e) {
      jjStopStringLiteralDfa_0(6, active0);
      return 7;
   }
   switch(curChar)
   {
      case 99:
         return jjMoveStringLiteralDfa8_0(active0, 0x1000000L);
      default :
         break;
   }
   return jjStartNfa_0(6, active0);
}
private int jjMoveStringLiteralDfa8_0(long old0, long active0)
{
   if (((active0 &= old0)) == 0L)
      return jjStartNfa_0(6, old0);
   try { curChar = input_stream.readChar(); }
   catch(java.io.IOException e) {
      jjStopStringLiteralDfa_0(7, active0);
      return 8;
   }
   switch(curChar)
   {
      case 101:
         return jjMoveStringLiteralDfa9_0(active0, 0x1000000L);
      default :
         break;
   }
   return jjStartNfa_0(7, active0);
}
private int jjMoveStringLiteralDfa9_0(long old0, long active0)
{
   if (((active0 &= old0)) == 0L)
      return jjStartNfa_0(7, old0);
   try { curChar = input_stream.readChar(); }
   catch(java.io.IOException e) {
      jjStopStringLiteralDfa_0(8, active0);
      return 9;
   }
   switch(curChar)
   {
      case 115:
         if ((active0 & 0x1000000L) != 0L)
            return jjStartNfaWithStates_0(9, 24, 13);
         break;
      default :
         break;
   }
   return jjStartNfa_0(8, active0);
}
private int jjStartNfaWithStates_0(int pos, int kind, int state)
{
   jjmatchedKind = kind;
   jjmatchedPos = pos;
   try { curChar = input_stream.readChar(); }
   catch(java.io.IOException e) { return pos + 1; }
   return jjMoveNfa_0(state, pos + 1);
}
static final long[] jjbitVec0 = {
   0xfffffffffffffffeL, 0xffffffffffffffffL, 0xffffffffffffffffL, 0xffffffffffffffffL
};
static final long[] jjbitVec2 = {
   0x0L, 0x0L, 0xffffffffffffffffL, 0xffffffffffffffffL
};
private int jjMoveNfa_0(int startState, int curPos)
{
   int startsAt = 0;
   jjnewStateCnt = 24;
   int i = 1;
   jjstateSet[0] = startState;
   int kind = 0x7fffffff;
   for (;;)
   {
      if (++jjround == 0x7fffffff)
         ReInitRounds();
      if (curChar < 64)
      {
         long l = 1L << curChar;
         do
         {
            switch(jjstateSet[--i])
            {
               case 0:
                  if ((0x3ff000000000000L & l) != 0L)
                  {
                     if (kind > 48)
                        kind = 48;
                     jjCheckNAddTwoStates(1, 15);
                  }
                  else if ((0x7000000000000000L & l) != 0L)
                  {
                     if (kind > 54)
                        kind = 54;
                  }
                  else if ((0x280000000000L & l) != 0L)
                     jjCheckNAdd(1);
                  else if (curChar == 33)
                     jjCheckNAdd(8);
                  else if (curChar == 41)
                  {
                     if (kind > 52)
                        kind = 52;
                  }
                  else if (curChar == 40)
                  {
                     if (kind > 51)
                        kind = 51;
                  }
                  else if (curChar == 39)
                     jjCheckNAddTwoStates(3, 4);
                  if (curChar == 60)
                     jjCheckNAdd(8);
                  else if (curChar == 62)
                     jjCheckNAdd(8);
                  break;
               case 1:
                  if ((0x3ff000000000000L & l) == 0L)
                     break;
                  if (kind > 48)
                     kind = 48;
                  jjCheckNAdd(1);
                  break;
               case 2:
                  if (curChar == 39)
                     jjCheckNAddTwoStates(3, 4);
                  break;
               case 3:
                  if ((0xffffff7bffffd9ffL & l) != 0L)
                     jjCheckNAddTwoStates(3, 4);
                  break;
               case 4:
                  if (curChar == 39 && kind > 49)
                     kind = 49;
                  break;
               case 5:
                  if (curChar == 40 && kind > 51)
                     kind = 51;
                  break;
               case 6:
                  if (curChar == 41 && kind > 52)
                     kind = 52;
                  break;
               case 7:
                  if ((0x7000000000000000L & l) != 0L && kind > 54)
                     kind = 54;
                  break;
               case 8:
                  if (curChar == 61 && kind > 54)
                     kind = 54;
                  break;
               case 9:
                  if (curChar == 62)
                     jjCheckNAdd(8);
                  break;
               case 10:
                  if (curChar == 60)
                     jjCheckNAdd(8);
                  break;
               case 11:
                  if (curChar == 33)
                     jjCheckNAdd(8);
                  break;
               case 14:
                  if ((0x3ff000000000000L & l) == 0L)
                     break;
                  if (kind > 48)
                     kind = 48;
                  jjCheckNAddTwoStates(1, 15);
                  break;
               case 15:
                  if ((0x3ff000000000000L & l) != 0L)
                     jjstateSet[jjnewStateCnt++] = 16;
                  break;
               case 16:
                  if ((0x3ff000000000000L & l) != 0L)
                     jjstateSet[jjnewStateCnt++] = 17;
                  break;
               case 17:
                  if ((0x3ff000000000000L & l) != 0L)
                     jjstateSet[jjnewStateCnt++] = 18;
                  break;
               case 18:
                  if (curChar == 45)
                     jjstateSet[jjnewStateCnt++] = 19;
                  break;
               case 19:
                  if ((0x3ff000000000000L & l) != 0L)
                     jjstateSet[jjnewStateCnt++] = 20;
                  break;
               case 20:
                  if ((0x3ff000000000000L & l) != 0L)
                     jjstateSet[jjnewStateCnt++] = 21;
                  break;
               case 21:
                  if (curChar == 45)
                     jjstateSet[jjnewStateCnt++] = 22;
                  break;
               case 22:
                  if ((0x3ff000000000000L & l) != 0L)
                     jjstateSet[jjnewStateCnt++] = 23;
                  break;
               case 23:
                  if ((0x3ff000000000000L & l) != 0L && kind > 50)
                     kind = 50;
                  break;
               default : break;
            }
         } while(i != startsAt);
      }
      else if (curChar < 128)
      {
         long l = 1L << (curChar & 077);
         do
         {
            switch(jjstateSet[--i])
            {
               case 0:
                  if ((0x7fffffe07fffffeL & l) == 0L)
                     break;
                  if (kind > 55)
                     kind = 55;
                  jjCheckNAdd(13);
                  break;
               case 3:
                  jjAddStates(0, 1);
                  break;
               case 13:
                  if ((0x7fffffe87fffffeL & l) == 0L)
                     break;
                  if (kind > 55)
                     kind = 55;
                  jjCheckNAdd(13);
                  break;
               default : break;
            }
         } while(i != startsAt);
      }
      else
      {
         int hiByte = (int)(curChar >> 8);
         int i1 = hiByte >> 6;
         long l1 = 1L << (hiByte & 077);
         int i2 = (curChar & 0xff) >> 6;
         long l2 = 1L << (curChar & 077);
         do
         {
            switch(jjstateSet[--i])
            {
               case 3:
                  if (jjCanMove_0(hiByte, i1, i2, l1, l2))
                     jjAddStates(0, 1);
                  break;
               default : break;
            }
         } while(i != startsAt);
      }
      if (kind != 0x7fffffff)
      {
         jjmatchedKind = kind;
         jjmatchedPos = curPos;
         kind = 0x7fffffff;
      }
      ++curPos;
      if ((i = jjnewStateCnt) == (startsAt = 24 - (jjnewStateCnt = startsAt)))
         return curPos;
      try { curChar = input_stream.readChar(); }
      catch(java.io.IOException e) { return curPos; }
   }
}
static final int[] jjnextStates = {
   3, 4, 
};
private static final boolean jjCanMove_0(int hiByte, int i1, int i2, long l1, long l2)
{
   switch(hiByte)
   {
      case 0:
         return ((jjbitVec2[i2] & l2) != 0L);
      default :
         if ((jjbitVec0[i1] & l1) != 0L)
            return true;
         return false;
   }
}

/** Token literal values. */
public static final String[] jjstrLiteralImages = {
"", null, null, null, null, null, "\73", "\56", "\52", "\145\170\151\164", 
"\143\162\145\141\164\145", "\144\162\157\160", "\164\141\142\154\145", "\164\141\142\154\145\163", 
"\163\150\157\167", "\144\145\163\143", "\163\145\154\145\143\164", "\151\156\163\145\162\164", 
"\151\156\164\157", "\146\162\157\155", "\144\145\154\145\164\145", 
"\160\162\151\155\141\162\171", "\146\157\162\145\151\147\156", "\153\145\171", 
"\162\145\146\145\162\145\156\143\145\163", "\151\163", "\156\157\164", "\156\165\154\154", "\166\141\154\165\145\163", 
"\141\163", "\157\162", "\141\156\144", "\167\150\145\162\145", "\151\156\164", 
"\143\150\141\162", "\144\141\164\145", "\142\154\157\167\165\160", null, null, null, null, null, 
null, null, null, null, null, null, null, null, null, null, null, "\54", null, null, };

/** Lexer state names. */
public static final String[] lexStateNames = {
   "DEFAULT",
};
static final long[] jjtoToken = {
   0xff001fffffffc1L, 
};
static final long[] jjtoSkip = {
   0x3eL, 
};
static final long[] jjtoSpecial = {
   0x3eL, 
};
protected SimpleCharStream input_stream;
private final int[] jjrounds = new int[24];
private final int[] jjstateSet = new int[48];
protected char curChar;
/** Constructor. */
public SQLParserTokenManager(SimpleCharStream stream){
   if (SimpleCharStream.staticFlag)
      throw new Error("ERROR: Cannot use a static CharStream class with a non-static lexical analyzer.");
   input_stream = stream;
}

/** Constructor. */
public SQLParserTokenManager(SimpleCharStream stream, int lexState){
   this(stream);
   SwitchTo(lexState);
}

/** Reinitialise parser. */
public void ReInit(SimpleCharStream stream)
{
   jjmatchedPos = jjnewStateCnt = 0;
   curLexState = defaultLexState;
   input_stream = stream;
   ReInitRounds();
}
private void ReInitRounds()
{
   int i;
   jjround = 0x80000001;
   for (i = 24; i-- > 0;)
      jjrounds[i] = 0x80000000;
}

/** Reinitialise parser. */
public void ReInit(SimpleCharStream stream, int lexState)
{
   ReInit(stream);
   SwitchTo(lexState);
}

/** Switch to specified lex state. */
public void SwitchTo(int lexState)
{
   if (lexState >= 1 || lexState < 0)
      throw new TokenMgrError("Error: Ignoring invalid lexical state : " + lexState + ". State unchanged.", TokenMgrError.INVALID_LEXICAL_STATE);
   else
      curLexState = lexState;
}

protected Token jjFillToken()
{
   final Token t;
   final String curTokenImage;
   final int beginLine;
   final int endLine;
   final int beginColumn;
   final int endColumn;
   String im = jjstrLiteralImages[jjmatchedKind];
   curTokenImage = (im == null) ? input_stream.GetImage() : im;
   beginLine = input_stream.getBeginLine();
   beginColumn = input_stream.getBeginColumn();
   endLine = input_stream.getEndLine();
   endColumn = input_stream.getEndColumn();
   t = Token.newToken(jjmatchedKind, curTokenImage);

   t.beginLine = beginLine;
   t.endLine = endLine;
   t.beginColumn = beginColumn;
   t.endColumn = endColumn;

   return t;
}

int curLexState = 0;
int defaultLexState = 0;
int jjnewStateCnt;
int jjround;
int jjmatchedPos;
int jjmatchedKind;

/** Get the next Token. */
public Token getNextToken() 
{
  Token specialToken = null;
  Token matchedToken;
  int curPos = 0;

  EOFLoop :
  for (;;)
  {
   try
   {
      curChar = input_stream.BeginToken();
   }
   catch(java.io.IOException e)
   {
      jjmatchedKind = 0;
      matchedToken = jjFillToken();
      matchedToken.specialToken = specialToken;
      return matchedToken;
   }

   jjmatchedKind = 0x7fffffff;
   jjmatchedPos = 0;
   curPos = jjMoveStringLiteralDfa0_0();
   if (jjmatchedKind != 0x7fffffff)
   {
      if (jjmatchedPos + 1 < curPos)
         input_stream.backup(curPos - jjmatchedPos - 1);
      if ((jjtoToken[jjmatchedKind >> 6] & (1L << (jjmatchedKind & 077))) != 0L)
      {
         matchedToken = jjFillToken();
         matchedToken.specialToken = specialToken;
         return matchedToken;
      }
      else
      {
         if ((jjtoSpecial[jjmatchedKind >> 6] & (1L << (jjmatchedKind & 077))) != 0L)
         {
            matchedToken = jjFillToken();
            if (specialToken == null)
               specialToken = matchedToken;
            else
            {
               matchedToken.specialToken = specialToken;
               specialToken = (specialToken.next = matchedToken);
            }
         }
         continue EOFLoop;
      }
   }
   int error_line = input_stream.getEndLine();
   int error_column = input_stream.getEndColumn();
   String error_after = null;
   boolean EOFSeen = false;
   try { input_stream.readChar(); input_stream.backup(1); }
   catch (java.io.IOException e1) {
      EOFSeen = true;
      error_after = curPos <= 1 ? "" : input_stream.GetImage();
      if (curChar == '\n' || curChar == '\r') {
         error_line++;
         error_column = 0;
      }
      else
         error_column++;
   }
   if (!EOFSeen) {
      input_stream.backup(1);
      error_after = curPos <= 1 ? "" : input_stream.GetImage();
   }
   throw new TokenMgrError(EOFSeen, curLexState, error_line, error_column, error_after, curChar, TokenMgrError.LEXICAL_ERROR);
  }
}

private void jjCheckNAdd(int state)
{
   if (jjrounds[state] != jjround)
   {
      jjstateSet[jjnewStateCnt++] = state;
      jjrounds[state] = jjround;
   }
}
private void jjAddStates(int start, int end)
{
   do {
      jjstateSet[jjnewStateCnt++] = jjnextStates[start];
   } while (start++ != end);
}
private void jjCheckNAddTwoStates(int state1, int state2)
{
   jjCheckNAdd(state1);
   jjCheckNAdd(state2);
}

}
