package com.drunkenhaze.dbms.components;

public class SelectedColumn {
	/*
	 * select 구문에서 선택된 칼럼들에 대한 정보를 저장하는 클래스
	 *
	 * - 칼럼명
	 * - 테이블 (옵션)
	 * - alias (옵션)
	 *
	 * case insensitive 속성을 유지하기 위해 선택된 칼럼명과 테이블명은 전부 lowercase로 저장된다.
	 */
	private String columnName;
	private String tableName;
	private String alias;

	public SelectedColumn() {
		columnName = null;
		tableName = null;
		alias = null;
	}

	public SelectedColumn(String columnName, String tableName, String alias) {
		this.columnName = columnName.toLowerCase();
		this.tableName = tableName == null ? null : tableName.toLowerCase();
		this.alias = alias;
	}

	public String getColumnName() {
		return columnName;
	}
	public void setColumnName(String columnName) {
		this.columnName = columnName.toLowerCase();
	}
	public String getTableName() {
		return tableName;
	}
	public void setTableName(String tableName) {
		this.tableName = tableName.toLowerCase();
	}
	public String getAlias() {
		return alias;
	}
	public void setAlias(String alias) {
		this.alias = alias;
	}
}
