package com.drunkenhaze.dbms.operators;

import java.util.List;
import java.util.Map;

import com.drunkenhaze.dbms.components.Row;
import com.drunkenhaze.dbms.components.TableSchema;
import com.drunkenhaze.dbms.exceptions.SemanticError;

public class AndOperator implements Operator {

	Operator operand1;
	Operator operand2;

	public AndOperator(Operator operator1, Operator operator2) {
		operand1 = operator1;
		operand2 = operator2;
	}

	@Override
	public EvalResult eval(List<Row> resultSet, List<TableSchema> targetTables,
			Map<String, Integer> tableIndexMap) throws SemanticError {
		EvalResult r1 = operand1.eval(resultSet, targetTables, tableIndexMap);
		EvalResult r2 = operand2.eval(resultSet, targetTables, tableIndexMap);
		if (r1 == EvalResult.FALSE || r2 == EvalResult.FALSE) return EvalResult.FALSE;
		else if (r1 == EvalResult.UNKNOWN || r2 == EvalResult.UNKNOWN) return EvalResult.UNKNOWN;
		else return EvalResult.TRUE;
	}

}
