package com.drunkenhaze.dbms.managers;

import java.io.File;

public class FileUtils {
	public static void deleteFilesInDirectory(File path) {
        if (!path.exists()) return;

        File[] files = path.listFiles();
        for (File file : files) {
            if (file.isDirectory()) {
                deleteFilesInDirectory(file);
            } else {
                file.delete();
            }
        }

        return;
    }

	public static void deleteDirectory(File path) {
		if (!path.exists()) return;

        File[] files = path.listFiles();
        for (File file : files) {
            if (file.isDirectory()) {
                deleteFilesInDirectory(file);
            } else {
                file.delete();
            }
        }
        path.delete();

        return;
	}

	public static void mkdirIfNotExists(File path) {
		if (!path.exists()) {
			path.mkdirs();
		}
	}
}
