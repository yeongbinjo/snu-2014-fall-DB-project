package com.drunkenhaze.dbms.main;

import java.io.StringReader;

import junit.framework.Test;
import junit.framework.TestCase;
import junit.framework.TestSuite;

import com.drunkenhaze.dbms.components.TableSchema;
import com.drunkenhaze.dbms.exceptions.SemanticError;
import com.drunkenhaze.dbms.managers.FileManager;

public class SemanticTest extends TestCase {
	FileManager fileMgr;
	SQLParser parser;

	/**
	 * Create the test case
	 *
	 * @param testName name of the test case
	 */
	public SemanticTest(String testName)
	{
		super( testName );
	}

	/**
	 * @return the suite of tests being tested
	 */
	public static Test suite()
	{
		return new TestSuite( SemanticTest.class );
	}

	public void setUp() {
		if (FileManager.getDBPath() == null)
			FileManager.setDBPath(Constants.TEST_DB_PATH);
		fileMgr = FileManager.getInstance();
		fileMgr.blowup();
		parser = new SQLParser(System.in);
		parser.init();
		parser.setFileManager(fileMgr);
	}

	private void testSemanticSuccessQuery(String testQuery, boolean stackTrace) {
		StringReader sr = new StringReader(testQuery);
		parser.ReInit(sr);
		try {
			boolean keepGoing = true;
			while (keepGoing) {
				keepGoing = parser.parse();
				if (parser.isStreamEmpty()) keepGoing = false;
			}
		} catch (ParseException e) {
			if (stackTrace) e.printStackTrace();
			System.out.println("Unexpected ParseException");
			fail();
		} catch (SemanticError e) {
			if (stackTrace) e.printStackTrace();
			System.out.println("Unexpected SemanticError");
			fail();
		}
	}

	private void testSemanticErrorQuery(String testQuery, String expectMsg, boolean stackTrace) {
		StringReader sr = new StringReader(testQuery);
		parser.ReInit(sr);
		try {
			boolean keepGoing = true;
			while (keepGoing) {
				keepGoing = parser.parse();
				if (parser.isStreamEmpty()) keepGoing = false;
			}
			fail();
		} catch (ParseException e) {
			if (stackTrace) e.printStackTrace();
			System.out.println("Unexpected ParseException");
			fail();
		} catch (SemanticError e) {
			if (stackTrace) e.printStackTrace();
			parser.printShell(e.getMessage());
			assertEquals(expectMsg, e.getMessage());
		}
	}

	/*
	 * Test for success query
	 */
	public void testBaseCase() {
		fileMgr.blowup();

		// CREATE TABLE
		testSemanticSuccessQuery("create table customer(\n" +
				"	cust_id char(6),\n" +
				"	customer_name char(30),\n" +
				"	customer_street char(20),\n" +
				"	customer_city char (20),\n" +
				"	primary key (cust_id)\n" +
				");\n", false);
		testSemanticSuccessQuery("desc customer;", false);
		testSemanticSuccessQuery("create table borrower(\n" +
				"	cust_id char(6),\n" +
				"	loan_number int,\n" +
				"	foreign key(cust_id) references customer(cust_id),\n" +
				"	primary key(loan_number)\n" +
				");", false);
		testSemanticSuccessQuery("create table account(\n" +
				"	account_number int not null,\n" +
				"	branch_name char(15),\n" +
				"	primary key(account_number)\n" +
				");", false);
		testSemanticSuccessQuery("create table branch(\n" +
				"	branch_name char(30) not null,\n" +
				"	assets int,\n" +
				"	branch_city char(20),\n" +
				"	primary key(branch_name)\n" +
				");\n", false);

		// SHOW TABLES
		testSemanticSuccessQuery("show tables;", false);

		// DESC TABLE
		testSemanticSuccessQuery("desc customer; desc borrower; desc account; desc branch;", false);

		// DROP TABLE
		testSemanticSuccessQuery("drop table account; drop table branch;", false);

		// DROP TABLE with right order
		testSemanticSuccessQuery("drop table borrower; drop table customer;", false);

		// CASE INSENSITIVE SUPPORT 1
		testSemanticSuccessQuery("create table customer(\n" +
				"	cust_id char(6),\n" +
				"	customer_name char(30),\n" +
				"	customer_street char(20),\n" +
				"	customer_city char (20),\n" +
				"	primary key (cust_id)\n" +
				");\n", false);
		testSemanticErrorQuery("create table Customer(\n" +
				"	cust_id char(6),\n" +
				"	customer_name char(30),\n" +
				"	customer_street char(20),\n" +
				"	customer_city char (20),\n" +
				"	primary key (cust_id)\n" +
				");\n", Messages.TableExistenceError, false);

		// CASE INSENSITIVE SUPPORT 2
		testSemanticSuccessQuery("create table borrower(\n" +
				"	cust_id char(6),\n" +
				"	loan_number int,\n" +
				"	foreign key(cust_id) references customer(Cust_Id),\n" +
				"	primary key(loan_number)\n" +
				");", false);

		// CASE INSENSITIVE SUPPORT 3
		testSemanticSuccessQuery("create table borrower_(\n" +
				"	cust_id char(6),\n" +
				"	loan_number int,\n" +
				"	foreign key(cust_id) references customer(Cust_Id),\n" +
				"	primary key(LOAN_NUMBER)\n" +
				");", false);

		// CASE INSENSITIVE SUPPORT 4
		testSemanticSuccessQuery("desc BoRrOwEr_;", false);

		// CASE INSENSITIVE SUPPORT 5
		testSemanticSuccessQuery("show tables;", false);

		// CASE INSENSITIVE SUPPORT 6
		testSemanticSuccessQuery("drop table bOrRoWeR_;", false);

		// PRIMARY KEY IS NOT NULL
		try {
			assertTrue(TableSchema.load("borrower").getColumn("loan_number").isNotNull());
		} catch (SemanticError e) {
			e.printStackTrace();
			fail();
		}

		// COLUMN DEFINITION AND KEY CONSTRAINTS DECLARATION MIXED
		testSemanticSuccessQuery("create table borrower_(\n" +
				"	foreign key(cust_id) references customer(Cust_Id),\n" +
				"	primary key(LOAN_NUMBER),\n" +
				"	cust_id char(6),\n" +
				"	LOAN_NuMBER int\n" +
				");", false);
		testSemanticSuccessQuery("desc borrower_;", false);

		// MULTIPLE REFERENCES with ONE FOREIGN KEY COLUMN
		testSemanticSuccessQuery("create table students(id int, primary key (id)); ", false);
		testSemanticSuccessQuery("create table students_other(id int, primary key (id)); ", false);
		testSemanticSuccessQuery("create table enrolls (\n" +
				"	id int,\n" +
				"	lecture_name char(50),\n" +
				"	foreign key(id) references students(id),\n" +
				"	foreign key(id) references students_other(id)\n" +
				");", true);

	}

	/*
	 * Test for semantic error query
	 */
	public void testSemanticErrorCase() {
		fileMgr.blowup();

		// ShowTablesNoTable
		testSemanticErrorQuery("show tables;",
				Messages.ShowTablesNoTable, false);

		// NoSuchTable
		testSemanticErrorQuery("desc account;",
				Messages.NoSuchTable, false);

		// DuplicateColumnDefError
		testSemanticErrorQuery("create table account(" +
				"account_number int not null," +
				"branch_name char(15)," +
				"account_number char(5)" +
				"primary key(account_number);",
				Messages.DuplicateColumnDefError, false);

		// DuplicatePrimaryKeyDefError
		testSemanticErrorQuery("create table account(account_number int not null," +
				"branch_name char(15)," +
				"primary key(account_number)," +
				"primary key(branch_name));",
				Messages.DuplicatePrimaryKeyDefError, false);

		// ReferenceTypeError 1
		testSemanticSuccessQuery("create table customer(\n" +
				"	cust_id char(6),\n" +
				"	customer_name char(30),\n" +
				"	customer_street char(20),\n" +
				"	customer_city char (20),\n" +
				"	primary key (cust_id)\n" +
				");\n", false);
		testSemanticErrorQuery("create table borrower(\n" +
				"	cust_id char(15),\n" +
				"	loan_number int,\n" +
				"	foreign key(cust_id) references customer(cust_id),\n" +
				"	primary key(loan_number)\n" +
				");",
				Messages.ReferenceTypeError, false);

		// ReferenceTypeError 2
		testSemanticErrorQuery("create table borrower(\n" +
				"	cust_id int,\n" +
				"	loan_number int,\n" +
				"	foreign key(cust_id) references customer(cust_id),\n" +
				"	primary key(loan_number)\n" +
				");",
				Messages.ReferenceTypeError, false);

		// ReferenceTypeError 3
		testSemanticErrorQuery("create table borrower(\n" +
				"	cust_id date,\n" +
				"	loan_number int,\n" +
				"	foreign key(cust_id) references customer(cust_id),\n" +
				"	primary key(loan_number)\n" +
				");",
				Messages.ReferenceTypeError, false);

		// ReferenceNonPrimaryKeyError
		testSemanticErrorQuery("create table borrower(\n" +
				"	cust_id char(6),\n" +
				"	customer_name char(30),\n" +
				"	loan_number int,\n" +
				"	foreign key(cust_id, customer_name) references customer(cust_id, customer_name),\n" +
				"	primary key(loan_number)\n" +
				");",
				Messages.ReferenceNonPrimaryKeyError, false);

		// ReferenceColumnExistenceError
		testSemanticErrorQuery("create table borrower(\n" +
				"	customer_id char(6),\n" +
				"	loan_number int,\n" +
				"	foreign key(customer_id) references customer(customer_id),\n" +
				"	primary key(loan_number)\n" +
				");",
				Messages.ReferenceColumnExistenceError, false);

		// ReferenceTableExistenceError
		testSemanticErrorQuery("create table borrower(\n" +
				"	cust_id char(6),\n" +
				"	loan_number int,\n" +
				"	foreign key(cust_id) references cust(cust_id),\n" +
				"	primary key(loan_number)\n" +
				");",
				Messages.ReferenceTableExistenceError, false);

		// ReferenceTableExistenceError - SELF REFERENCE
		testSemanticErrorQuery("create table student(\n" +
				"	student_id char(10),\n" +
				"	student_name char(20),\n" +
				"	foreign key(student_id) references student(student_id));",
				Messages.ReferenceTableExistenceError, false);


		// NonExistingColumnDefError(#colName)
		testSemanticErrorQuery("create table borrower(\n" +
				"	customer_id char(6),\n" +
				"	loan_number int,\n" +
				"	foreign key(cust_id) references customer(cust_id),\n" +
				"	primary key(loan_number)\n" +
				");",
				String.format(Messages.NonExistingColumnDefError, "cust_id"), false);

		// TableExistenceError
		testSemanticErrorQuery("create table customer(\n" +
				"	cust_id char(6),\n" +
				"	customer_name char(30),\n" +
				"	customer_street char(20),\n" +
				"	customer_city char (20),\n" +
				"	primary key (cust_id)\n" +
				");\n",
				Messages.TableExistenceError, false);

		// DropReferencedTableError(#tabName)
		testSemanticSuccessQuery("create table borrower(\n" +
				"	cust_id char(6),\n" +
				"	loan_number int,\n" +
				"	foreign key(cust_id) references customer(cust_id),\n" +
				"	primary key(loan_number)\n" +
				");", false);
		testSemanticErrorQuery("drop table customer;",
				String.format(Messages.DropReferencedTableError, "customer"), false);

		// CharLengthError
		testSemanticErrorQuery("create table a(b char(0));",
				Messages.CharLengthError, false);

	}

	/*
	 * Test for complex cases
	 */
	public void testComplexCase() {
		fileMgr.blowup();

		testSemanticSuccessQuery("create table department (\n" +
				"		dept_id		char(30), \n" +
				"		dept_name 	char(30), \n" +
				"		office		char(30),  \n" +
				"		primary key(dept_id)\n" +
				");\n" +
				"create table student (\n" +
				"		stu_id 		char(30),\n" +
				"		resident_id 	char(42) not null,\n" +
				"		name 		char(30) not null,\n" +
				"		year 		int,\n" +
				"		address 		char(30),\n" +
				"		dept_id 		char(30),\n" +
				"		primary key(stu_id),\n" +
				"		foreign key(dept_id) references \n" +
				"					department(dept_id)\n" +
				");\n" +
				"create table professor 	(\n" +
				"		prof_id 	char(30) ,\n" +
				"		resident_id 	char(42) 	not null,\n" +
				"		name 		char(30) 	not null,\n" +
				"		dept_id 	char(30),\n" +
				"		position 	char(30),\n" +
				"		year_emp 	int,\n" +
				"		primary key(prof_id),\n" +
				"		foreign key(dept_id) \n" +
				"				references department(dept_id)\n" +
				");\n" +
				"create table course (\n" +
				"		course_id 	char(30) ,\n" +
				"		title 		char(42) 	not null,\n" +
				"		credit 		int,\n" +
				"		primary key(course_id)\n" +
				");\n" +
				"create table class (\n" +
				"		class_id 	char(30) ,\n" +
				"		course_id 	char(30),\n" +
				"		year 		int,\n" +
				"		semester	int,\n" +
				"		division	char(1),\n" +
				"		prof_id 	char(30),\n" +
				"		classroom 	char(9),\n" +
				"		enroll 		int,\n" +
				"		primary key(class_id),\n" +
				"		foreign key(course_id) \n" +
				"				references  	course(course_id),\n" +
				"		foreign key(prof_id) \n" +
				"				references 	professor(prof_id)\n" +
				");\n" +
				"create table takes (\n" +
				"		stu_id 		char(30) ,\n" +
				"		class_id 	char(30),\n" +
				"		grade 		char(5),\n" +
				"		primary key(stu_id, class_id),\n" +
				"		foreign key(stu_id) \n" +
				"				references 	student(stu_id),\n" +
				"		foreign key(class_id) \n" +
				"				references 	class(class_id)\n" +
				");\n", false);
		testSemanticSuccessQuery("show tables;", false);
		testSemanticSuccessQuery("desc department; desc student; desc professor; desc course; desc class; desc takes;", false);

		testSemanticErrorQuery("drop table department;",
				String.format(Messages.DropReferencedTableError, "department"), false);

		testSemanticErrorQuery("drop table professor;",
				String.format(Messages.DropReferencedTableError, "professor"), false);

		testSemanticErrorQuery("drop table student;",
				String.format(Messages.DropReferencedTableError, "student"), false);

		testSemanticErrorQuery("drop table course;",
				String.format(Messages.DropReferencedTableError, "course"), false);

		testSemanticErrorQuery("drop table class;",
				String.format(Messages.DropReferencedTableError, "class"), false);

		testSemanticSuccessQuery("drop table takes;", false);

		testSemanticErrorQuery("drop table department;",
				String.format(Messages.DropReferencedTableError, "department"), false);

		testSemanticErrorQuery("drop table professor;",
				String.format(Messages.DropReferencedTableError, "professor"), false);

		testSemanticSuccessQuery("drop table student;", false);

		testSemanticErrorQuery("drop table course;",
				String.format(Messages.DropReferencedTableError, "course"), false);

		testSemanticSuccessQuery("drop table class;", false);

		testSemanticErrorQuery("drop table department;",
				String.format(Messages.DropReferencedTableError, "department"), false);

		testSemanticSuccessQuery("drop table professor;", false);

		testSemanticSuccessQuery("drop table course;", false);

		testSemanticSuccessQuery("drop table department;", false);

		testSemanticErrorQuery("show tables;", Messages.ShowTablesNoTable, false);
	}
}
