package com.drunkenhaze.dbms.main;

import java.io.StringReader;

import junit.framework.Test;
import junit.framework.TestCase;
import junit.framework.TestSuite;

import com.drunkenhaze.dbms.exceptions.SemanticError;

/**
 * Unit test for SQLParser
 */
public class SQLParserTest
	extends TestCase
{
	SQLParser parser;

	/**
	 * Create the test case
	 *
	 * @param testName name of the test case
	 */
	public SQLParserTest( String testName )
	{
		super( testName );
	}

	/**
	 * @return the suite of tests being tested
	 */
	public static Test suite()
	{
		return new TestSuite( SQLParserTest.class );
	}

	public void setUp() {
		parser = new SQLParser(System.in);
		parser.init();
	}

	private void testSuccessQuery(String testQuery, boolean stackTrace) {
		StringReader sr = new StringReader(testQuery);
		parser.ReInit(sr);
		try {
			boolean keepGoing = true;
			while (keepGoing) {
				keepGoing = parser.parse();
				if (parser.isStreamEmpty()) keepGoing = false;
			}
		} catch (ParseException e) {
			if (stackTrace) e.printStackTrace();
			System.out.println("Unexpected ParseException");
			fail();
		} catch (SemanticError e) {
			if (stackTrace) e.printStackTrace();
			System.out.println("Unexpected SemanticError");
			fail();
		}
	}

	private void testParseExceptionQuery(String testQuery, boolean stackTrace) {
		StringReader sr = new StringReader(testQuery);
		parser.ReInit(sr);
		try {
			boolean keepGoing = true;
			while (keepGoing) {
				keepGoing = parser.parse();
				if (parser.isStreamEmpty()) keepGoing = false;
			}
			fail();
		} catch (ParseException e) {
			if (stackTrace) e.printStackTrace();
			parser.printShell("Syntax Error");
		} catch (SemanticError e) {
			if (stackTrace) e.printStackTrace();
			System.out.println("Unexpected SemanticError");
			fail();
		}
	}

	/**
	 * Basic test for SQL Parser
	 *
	 * Test Cases
	 *
	 * - CREATE TABLE
	 * - DROP TABLE
	 * - SHOW TABLES
	 * - DESC
	 * - INSERT
	 * - DELETE
	 * - SELECT
	 * - (QUERY LIST)
	 * - ETC...
	 */
	public void testBasic() {
		// CREATE TABLE 1
		testSuccessQuery("create table account(account_number int not null, branch_name char(15), primary key(account_number));", false);

		// CREATE TABLE 2
		testSuccessQuery("create table member(id int not null, name char(32), reg_date date);", false);

		// DROP TABLE
		testSuccessQuery("drop table account;", false);

		// SHOW TABLES
		testSuccessQuery("show tables;", false);

		// INSERT 1
		testSuccessQuery("insert into account values(9732, 'Perryridge');", false);

		// INSERT 2
		testSuccessQuery("insert into member values(123, 'Youngki Park', 2010-09-08);", false);

		// DELETE
		testSuccessQuery("delete from account where branch_name = 'Perryridge';", false);

		// SELECT
		testSuccessQuery("select * from account;", false);

		// QUERY LIST
		testSuccessQuery("insert into account values(9732, 'Perryridge'); show tables; desc account; drop table account;", false);

		// EXIT
		testSuccessQuery("exit;", false);

		// QUERY LIST with EXIT 1
		testSuccessQuery("insert into account values(9732, 'Perryridge'); show tables; desc account; exit; drop table account;", false);

		// QUERY LIST with EXIT 2
		testSuccessQuery("insert into account values(9732, 'Perryridge'); show tables; desc account; drop table account; exit;", false);

		// TOO MUCH SPACE
		testSuccessQuery("						show\n" +
				"	     tables										\n" +
				"				;", false);

		// SPACE BETWEEN 1
		testSuccessQuery("create table account(branch_name char(15)	not null);", false);

		// SPACE BETWEEN 2
		testSuccessQuery("create table\n" +
				"account	( branch_name char (	15 )	not null ) ; ", false);

		// SPACE BETWEEN 3
		testSuccessQuery("create table account(branch_name char(15), foreign key(name) references client(name));", false);

		// COMPLEX SELECT 1
		testSuccessQuery("select id, m.name, m.join_date\n" +
				"from member as m, admin\n" +
				"where admin.id > 100 and m.join_date > 2010-01-01;", false);

		// COMPLEX SELECT 2
		testSuccessQuery("select id, m.name, m.join_date\n" +
				"from member as m, admin as ad\n" +
				"where (ad.id > 100) and (m.join_date!=2010-01-01);", false);
	}


	/**
	 * Test for Newline Processing
	 */
	public void testNewlineProcessing() {
		// LINE FEED (LINUX)
		testSuccessQuery("create table account (\n" +
				"\taccount_number int not null,\n" +
				"\tbranch_name char(15),\n" +
				"\tprimary key(account_number)\n" +
				");", false);

		// LINE FEED + CARRIAGE RETURN (WINDOWS)
		testSuccessQuery("insert into account values (\r\n" +
				"9732,\r\n" +
				"'Perryridge'\r\n" +
				");\r\n", false);

		// CARRIAGE RETURN (MAC OS X)
		testSuccessQuery("select customer_name, borrower.loan_number, amount from borrower, loan\r" +
				"where borrower.loan_number = loan.loan_number and\r" +
				"branch_name = 'Perryridge';", false);

		// WIERD CASE
		testSuccessQuery("select\n" +
				"customer_name,\n" +
				"borrower.loan_number,\n" +
				"amount\n" +
				"from\n" +
				"borrower,\n" +
				"loan\n" +
				"where\n" +
				"borrower.loan_number\n" +
				"=\n" +
				"loan.loan_number\n" +
				"and\r" +
				"branch_name\n" +
				"=\n" +
				"'Perryridge';", false);
	}

	/**
	 * Test for parse exception case
	 */
	public void testParseErrorCase() {
		// ILLEGAL IDENTIFIER 1
		testParseExceptionQuery("drop table not;", false);

		// ILLEGAL IDENTIFIER 2
		testParseExceptionQuery("drop table NoT;", false);

		// UNSUPPORTED QUERY
		testParseExceptionQuery("rename table teachers to instructors;", false);

		// CLUELESS
		testParseExceptionQuery("hello world;", false);

		// EMPTY QUERY 1
		testParseExceptionQuery(";", false);

		// EMPTY QUERY 2
		testParseExceptionQuery(";show tables;", false);

		// EMPTY QUERY with BLANK 1
		testParseExceptionQuery("\t;", false);

		// EMPTY QUERY with BLANK 2
		testParseExceptionQuery("\n;", false);

		// QUERY LIST with EMPTY QUERY 1
		testParseExceptionQuery("insert into account values(9732, 'Perryridge');; show tables; desc account; drop table account;", false);

		// QUERY LIST with EMPTY QUERY 2
		testParseExceptionQuery("insert into account values(9732, 'Perryridge'); show tables; desc account; drop table account;;", false);

		// CASE SENSITIVE 1
		testParseExceptionQuery("DROP table account", false);

		// CASE SENSITIVE 2
		testParseExceptionQuery("drop tAble account", false);

		// CASE SENSITIVE 3
		testParseExceptionQuery("show tableS;", false);

		// TABLENAME.COLUMNNAME
		testParseExceptionQuery("select id, m . name, m.join_date\n" +
				"from member as m, admin\n" +
				"where admin.id > 100;", false);

		// NO BLANK 1
		testParseExceptionQuery("create table account(branch_name char(15) notnull);", false);

		// NO BLANK 2
		testParseExceptionQuery("create table account(branch_name char(15)not null);", false);

		// NO BLANK 3
		testParseExceptionQuery("create table account(branch_name char(15), foreign key(name)references client(name));", false);
	}

}
