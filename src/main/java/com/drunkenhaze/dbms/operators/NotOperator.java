package com.drunkenhaze.dbms.operators;

import java.util.List;
import java.util.Map;

import com.drunkenhaze.dbms.components.Row;
import com.drunkenhaze.dbms.components.TableSchema;
import com.drunkenhaze.dbms.exceptions.SemanticError;

public class NotOperator implements Operator {

	Operator operand;

	public NotOperator(Operator operand) {
		this.operand = operand;
	}

	@Override
	public EvalResult eval(List<Row> resultSet, List<TableSchema> targetTables,
			Map<String, Integer> tableIndexMap) throws SemanticError {
		EvalResult r = operand.eval(resultSet, targetTables, tableIndexMap);
		if (r == EvalResult.TRUE) return EvalResult.FALSE;
		else if (r == EvalResult.FALSE) return EvalResult.TRUE;
		else return EvalResult.UNKNOWN;
	}

}
